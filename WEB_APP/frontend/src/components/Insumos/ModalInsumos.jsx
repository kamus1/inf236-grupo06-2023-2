import React, { useState, useEffect } from "react";
import "./ModalInsumos.css"
import { showStock } from "../../hooks/showStock";

function ModalInsumos({ idinsumo, isOpen, onClose }) {
  const [formData, setFormData] = useState({
    nombre: "",
    material: "",
    detalle: "",
    n_disponible: "",
    n_reservado: "",
    n_usado: "",
  });
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [successMessage, setSuccessMessage] = useState("");
  const [material, setMaterial] = useState("")
  const [detalle, setDetalle] = useState("")

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch(`http://localhost:8080/api/stock/insumo/${idinsumo}`);
        const insumos = await response.json();

        setMaterial(insumos[0].material)
        setDetalle(insumos[0].detalle)

        setFormData({
          nombre: insumos[0].nombre,
          material: insumos[0].material,
          detalle: insumos[0].detalle,
          n_disponible: insumos[0].n_disponible,
          n_reservado: insumos[0].n_reservado,
          n_usado: insumos[0].n_usado,
        });
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    if (isOpen) {
      fetchData();
    }
  }, [idinsumo, isOpen]);
  

  const handleSubmit = (e) => {
    e.preventDefault();

    const quantityToAdd = parseFloat(formData.sumar);

    if (isNaN(quantityToAdd)) {
      setIsSubmitting(false);
      setSuccessMessage("Coloque una cantidad válida");
      return;
    }

    const newNDisponible = parseFloat(formData.n_disponible) + quantityToAdd;
    setIsSubmitting(true);
    setSuccessMessage("Cantidad actualizada con éxito")

    fetch(`http://localhost:8080/api/stock/insumo/${idinsumo}`,{
            method:"PUT",
            headers: {
              'Content-Type': 'application/json',
            },
            body:JSON.stringify(
                {
                    nombre: formData.nombre,
                    material: formData.material,
                    detalle: formData.detalle,
                    cantidad: parseInt(formData.cantidad, 10) || 0,
                    n_disponible: parseInt(newNDisponible, 10) || 0,
                    n_reservado: parseInt(formData.n_reservado, 10) || 0,
                    n_usado: parseInt(formData.n_usado, 10) || 0,
                }
            )
        })
            .then(res=>res.json())
            .then(json=>console.log(json))
    window.location.reload();
  };

  const handleChange = (event) => {
    const { name, value } = event.target;

    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleClick = () => {
    onClose();
    setIsSubmitting(false);
    setSuccessMessage("");
    setFormData({
        nombre: "",
        material: "",
        detalle: "",
        n_disponible: "",
        n_reservado: "",
        n_usado: "",
    });
  };

  return (
    isOpen && (
      <div className="modal-overlay">
        <div className="modal">
          <section className="act-zone overflowy">
            <h1>Actualizar Insumos</h1>
            <form method="post" onSubmit={handleSubmit}>
              
              <input
                type="text"
                name="sumar"
                placeholder="Cantidad"
                autoComplete="off"
                defaultValue={"0"}
                onChange={handleChange}
              />

              <button
                type="submit"
                className={"newinsumo-btn"}
                disabled={isSubmitting}
              >
                Actualizar Cantidad
              </button>
              {successMessage && (
                <p className="success-message">{successMessage}</p>
              )}
            </form>
          </section>
          <section className="buttons-zone">
            <h2>Veysa Manualidades</h2>
            <hr />
            <p className="material-information">Material: {material}</p>
            <p className="detalle-information">Detalle: {detalle}</p>
            <button className="btn-border-red" onClick={handleClick}>
              Cerrar
            </button>
            <hr />
          </section>
        </div>
      </div>
    )
  );
}

export default ModalInsumos;
