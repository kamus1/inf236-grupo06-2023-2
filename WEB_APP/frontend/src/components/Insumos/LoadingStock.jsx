import React from "react";
import "./LoadingStock.css"; // Asegúrate de importar tu archivo CSS con los estilos

const LoadingStock = () => {
  return (
    <div className="loading-container">
      <div className="loading-spinner"></div>
      <p className="loading-text">Cargando...</p>
    </div>
  );
};

export default LoadingStock;
