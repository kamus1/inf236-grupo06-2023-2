const database = require('../db');


//Esta tabla es solo un ejemplo para que puedan ver comoa se hacen querys a la bdd, después puede ser eliminada

const createStock = async (req, res) => {
    const {id_tienda, nombre, material, detalle, cantidad} = req.body

    // Verificaciones de tipo de datos
    if (typeof id_tienda !== 'number' ||
    typeof nombre !== 'string' ||
    typeof material !== 'string' ||
    typeof detalle !== 'string' ||
    typeof cantidad !== 'number') {
    return res.status(400).json({'message': 'Error: tipo de dato incorrecto'});
    }

    const query = `INSERT INTO insumos (id_tienda, nombre, material, detalle, n_disponible) VALUES (${id_tienda}, "${nombre}", "${material}", "${detalle}", ${cantidad})`;

    try {
        database.query(query, (err) => {
            if (err) throw err;
            res.json({'message': "Elemento insertado con éxito!"})
        });
    } catch (err) {
        res.json({'message': err})
    }
    
};

const getStock = async (req, res) => {

    const query = 'Select * FROM insumos WHERE del = 0';

    try {
        database.query(query, (err, result) => {
            if (err) throw err;
            res.json(result)
        })
    } catch (err) {
        res.json({'message': err})
    }
};

const getStockById = async (req, res) => {
    const {id} = req.params
    const query = `Select * FROM insumos WHERE id = ${id}`;

    try {
        database.query(query, (err, result) => {
            if (err) throw err;
            res.json(result)
        })
    } catch (err) {
        res.json({'message': err})
    }
};

const getStockSearchByName = async (req, res) => {
    const idTienda = req.query.id_tienda;
    const nombre = req.query.nombre;

    // Verificaciones de tipo de datos
    if (typeof nombre !== 'string') {
        return res.status(400).json({'message': 'Error: tipo de dato incorrecto'});
    }

    let busqueda = nombre.replaceAll('-', ' ');

    const query = `SELECT * FROM insumos WHERE id_tienda = ${idTienda} AND nombre LIKE "%${busqueda}%"`;

    try {
        database.query(query, [idTienda], (err, result) => {
            if (err) throw err
            res.json(result)
        });
    } catch (err) {
        res.json({'message': err});
    }
};

const getStockByName = async (req, res) => {
    var nombr  = req.query.nombre

    // Verificaciones de tipo de datos
    if (typeof nombr !== 'string') {
        return res.status(400).json({'message': 'Error: tipo de dato incorrecto'});
    }
    
    let busqueda = nombr.replaceAll('-', ' ');
    const query = `Select * FROM insumos WHERE nombre = "${busqueda}"`;

    try {
        database.query(query, (err, result) => {
            if (err) throw err;
            res.json(result)
        })
    } catch (err) {
        res.json({'message': err})
    }
};

const updateStockById = async (req, res) => {
    const {id} = req.params
    const {nombre, material, detalle, n_disponible, n_reservado, n_usado} = req.body

    // Verificaciones de tipo de datos
    if (typeof nombre !== 'string' ||
    typeof material !== 'string' ||
    typeof detalle !== 'string' ||
    typeof n_disponible !== 'number' ||
    typeof n_reservado !== 'number' ||
    typeof n_usado !== 'number') {
    return res.status(400).json({'message': 'Error: tipo de dato incorrecto'});
    }

    const query = `UPDATE insumos SET nombre= "${nombre}", material= "${material}" , detalle= "${detalle}", n_disponible = ${n_disponible}, n_reservado = ${n_reservado}, n_usado = ${n_usado} WHERE id = ${id}`;

    try {
        database.query(query, (err,result) => {
            if (err) throw err;
            res.json(result)
        })
        
    } catch (err) {
        res.json({'message': err})
    }
};


const deleteStockById = async (req, res) => {
    const {id} = req.params

    const query = `UPDATE insumos SET del = 1 WHERE id = ${id}`;

    try {
        database.query(query, (err,result) => {
            if (err) throw err;
            res.json(result)
        })
        
    } catch (err) {
        res.json({'message': err})
    }
};

const getStockByTiendaId = async (req, res) => {
    const {id} = req.params
    const query = `Select * FROM insumos WHERE id_tienda = ${id} AND del = 0`;

    try {
        database.query(query, (err, result) => {
            if (err) throw err;
            res.json(result)
        })
    } catch (err) {
        res.json({'message': err})
    }
};

module.exports = {
    createStock, 
    getStock,
    getStockById,
    getStockByName,
    getStockSearchByName,
    updateStockById,
    deleteStockById,
    getStockByTiendaId
};
