const express = require('express');
const router = new express.Router();

const index =  require('../controllers/index');


// Esto es un ejemplo - SE PUEDE BORRAR
router.get('/', (req, res) => res.json({message: 'Probando... La prueba fue un éxito BACKEND! :P'}));

//insumos
router.get('/backend/insumos', index.getInsumos);
router.get('/backend/insumos/:id', index.getInsumosById);
router.get('/backend/insumos/search/:id_tienda/:search/', index.getInsumosSearchByName);
router.post('/backend/insumos', index.createStock);
router.get('/backend/insumos/tienda/:id', index.getInsumosByTiendaId);


//productos
router.get('/backend/productos', index.getProductos);
router.get('/backend/productos/search/:id_tienda/:search', index.getProductosByName);
router.get('/backend/productos/:id', index.getProductoById);
router.post('/backend/productos', index.createProducto);
router.post('/backend/productos/:id_producto/fabricar', index.fabricarProducto);
router.get('/backend/productos/tienda/:id', index.getProductosByTiendaId);

//fabricacion
router.get('/backend/fabricacion/activos', index.getProceso);
router.get('/backend/fabricacion/terminados', index.getProcesoTerminado);
router.put('/backend/fabricacion/:id', index.fabricarProductoTerminar)
router.delete('/backend/fabricacion/:id', index.fabricarProductoDelete);
router.get('/backend/fabricacion/tienda/:id', index.getProcesoByTiendaId);

router.get('/backend/fabricacion/activos/tienda/:id', index.getProcesosActivosByTiendaId);
router.get('/backend/fabricacion/terminados/tienda/:id', index.getProcesosTerminadosByTiendaId);

//tiendas
router.get('/backend/tiendas', index.getTienda);
router.get('/backend/tiendas/:id', index.getTiendaById);
router.get('/backend/tiendas/search/:search', index.getTiendaByName);
router.post('/backend/tiendas', index.createTienda);
router.put('/backend/tiendas/:id', index.updateTiendaById);
router.delete('/backend/tiendas/:id', index.deleteTiendaById);

module.exports = router;