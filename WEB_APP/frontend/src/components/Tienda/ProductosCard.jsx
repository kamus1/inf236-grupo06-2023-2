import './ProductosCard.css';

// Función para convertir una cadena a formato camelCase
function toTitleCase(text) {
    return text.toLowerCase().replace(/\b\w/g, firstLetter => firstLetter.toUpperCase());
  }

function ProductosCard({ img, desc, name, price }) {
  const formattedDesc = toTitleCase(desc);
  const formattedName = toTitleCase(name);

  return (
    <div className="box-productoscard">
      <img src={img} alt={desc} />
      <p className="desc-productoscard">{formattedDesc}</p>
      <p className="name-productoscard">{formattedName}</p>
      <p>${price}</p>
    </div>
  );
}

export default ProductosCard;
