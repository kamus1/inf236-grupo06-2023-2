import { Link } from "react-router-dom";
import {useParams} from "react-router-dom";
import { useState } from "react";

import "./NewInsumo.css";

function NewInsumo() {

  const { nombreTienda, idTienda} = useParams();

  const [formData, setFormData] = useState();
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [successMessage, setSuccessMessage] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    const isQuantityValid = !isNaN(Number(formData.cantidad));
    
    if (!isQuantityValid) {
      setIsSubmitting(false);
      setSuccessMessage("Coloque una cantidad válida");
      return;
    }
    
    setIsSubmitting(true);
    setSuccessMessage("Insumo agregado con éxito");

    fetch('http://localhost:8080/api/stock',{
            method:"POST",
            headers: {
              'Content-Type': 'application/json',
            },
            body:JSON.stringify(
                { 
                    id_tienda: parseInt(idTienda),
                    nombre: formData.nombre,
                    material: formData.material,
                    detalle: formData.detalle,
                    cantidad: parseInt(formData.cantidad, 10) || 0
                }
            )
        })
            .then(res=>res.json())
            .then(json=>console.log(json))
    };

  const handleChange = (event) => {
    const { name, value } = event.target;
    setFormData((prevData) => ({ ...prevData, [name]: value }));
  };

  const handleClick = () => {
    setIsSubmitting(false);
    setSuccessMessage("");
  };

  return (
    <div className="conteiner">
      <section className="white-section center">
        <h1>Agregar nuevo Insumo</h1>
        <form method="post" onSubmit={handleSubmit}>
          <input
            type="text"
            name="nombre"
            placeholder="Nombre insumo"
            onChange={handleChange}
            autoComplete="off"
            required
          />
          <input
            type="text"
            name="cantidad"
            placeholder="Cantidad"
            onChange={handleChange}
            autoComplete="off"
            required
          />
          <input
            type="text"
            name="material"
            placeholder="Material"
            onChange={handleChange}
            autoComplete="off"
            required
          />
          <input
            type="text"
            name="detalle"
            placeholder="Detalle"
            onChange={handleChange}
            autoComplete="off"
            required
          />
          <button
            type="submit"
            className={`newinsumo-btn ${
              isSubmitting ? "hidden" : "visible-inline"
            }`}
            disabled={isSubmitting}
          >
            Agregar insumo
          </button>
          <button type="reset" className={`newinsumo-btn ${
              isSubmitting ? "visible-inline" : "hidden"
            }`} onClick={handleClick}>
            Agregar nuevo insumo
          </button>
          {successMessage && (
            <p className="success-message">{successMessage}</p>
          )}
        </form>
        <hr className="green-hr"/>
      </section>
      <section className="red-section">
        <h2>{nombreTienda}</h2>
        <hr className="white-hr"/>
        <div className="link-conteiner">
          <Link to={`/tienda/${nombreTienda}/${idTienda}/gestion/insumos`} className="btn">
            Volver a Stock de Insumos
          </Link>
        </div>
      </section>
    </div>
  );
}

export default NewInsumo;
