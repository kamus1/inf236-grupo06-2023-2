const database = require('../db');

//Categorias 
const createTiendaCategoria = async (req, res) => {
    const {id_tienda, id_categoria} = req.body

    // Verificaciones de tipo de datos
    if (typeof id_tienda !== 'number' ||
    typeof id_categoria !== 'number') {
    return res.status(400).json({'message': 'Error: tipo de dato incorrecto'});
    }

    const query = `INSERT INTO tiendas_categorias(id_tienda, id_categoria) VALUES (${id_tienda}, ${id_categoria})`;

    try {
        database.query(query, (err, result) => {
            if (err) throw err;
            res.json({'message': "Elemento insertado con éxito!"})
        });
    } catch (err) {
        res.json({'message': err})
    }
};

const getTiendasCategorias = async (req, res) => {
    const query = 'Select * FROM tiandas_categorias WHERE del = 0';
        try {
            database.query(query, (err, result) => {
                if (err) throw err;
                res.json(result)
            })
        } catch (err) {
            res.json({'message': err})
        }
};

const getTiendaCategoriaById = async (req, res) => {
    const {id} = req.params

    const query = `Select * FROM tiendas_categorias WHERE id = ${id}`;

    try {
        database.query(query, (err, result) => {
            if (err) throw err;
            res.json(result)
        })
    } catch (err) {
        res.json({'message': err})
    }
};

const getTiendaCategoriaByIdTienda = async (req, res) => {
    const {id_tienda} = req.params

    const query = `Select * FROM tiendas_categorias WHERE id_tienda = ${id_tienda}`;

    try {
        database.query(query, (err, result) => {
            if (err) throw err;
            res.json(result)
        })
    } catch (err) {
        res.json({'message': err})
    }
};

const updateTiendaCategoriaById = async (req, res) => {
    const {id} = req.params
    const {id_tienda, id_categoria} = req.body

    // Verificaciones de tipo de datos
    if (typeof id_tienda !== 'number' ||
    typeof id_categoria !== 'number') {
    return res.status(400).json({'message': 'Error: tipo de dato incorrecto'});
    }

    const query = `UPDATE tiendas_categorias SET id_tienda = ${id_tienda}, id_categoria = ${id_categoria} WHERE id = ${id}`;

    try {
        database.query(query, (err, result) => {
            if (err) throw err;
            res.json(result);
        })
    } catch (err) {
        res.json({'message': err})
    }
}

const deleteTiendaCategoriaById = async (req, res) => {
    const {id} = req.params

    const query = `UPDATE tiendas_categorias SET del = 1 WHERE id = ${id}`;

    try {
        database.query(query, (err, result) => {
            if (err) throw err;
            res.json(result);
        })
    } catch (err) {
        res.json({'message': err})
    }
}

module.exports = {
    createTiendaCategoria,
    getTiendasCategorias,
    getTiendaCategoriaById,
    getTiendaCategoriaByIdTienda,
    updateTiendaCategoriaById,
    deleteTiendaCategoriaById
};