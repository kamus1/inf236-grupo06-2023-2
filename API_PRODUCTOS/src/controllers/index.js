const database = require('../db');


const createTableProductos = (req, res) => {
    const query =  'CREATE TABLE IF NOT EXISTS productos(id int AUTO_INCREMENT, id_tienda INT, nombre VARCHAR(100), precio INT, descripcion TINYTEXT, url_imagen TEXT, estado BOOL DEFAULT 1, del BOOL DEFAULT 0, PRIMARY KEY(id))';

    database.query(query, (err) => {
        if (err) throw err;
        res.send('Tabla creada')
    });
};

const createTableProductosInsumos = (req, res) => {
    const query =  'CREATE TABLE IF NOT EXISTS productos_insumos(id INT AUTO_INCREMENT, id_producto INT, id_tienda INT, material VARCHAR(50) NOT NULL, cantidad INT NOT NULL, del BOOL DEFAULT 0, FOREIGN KEY(id_producto) REFERENCES productos(id) ON DELETE CASCADE, PRIMARY KEY(id))';

    database.query(query, (err) => {
        if (err) throw err;
        res.send('Tabla creada')
    });
};

const createTableProcesoFabricacion = (req,res) => {
    const query =  'CREATE TABLE IF NOT EXISTS fabricacion(id INT AUTO_INCREMENT, id_producto INT, id_tienda INT, date_inicio DATE NOT NULL, date_termino DATE DEFAULT NULL, terminado BOOL DEFAULT 0, del BOOL DEFAULT 0, FOREIGN KEY(id_producto) REFERENCES productos(id) ON DELETE CASCADE, PRIMARY KEY(id))';

    database.query(query, (err) => {
        if (err) throw err;
        res.send('Tabla creada')
    });

};


module.exports = {
    createTableProductos,
    createTableProductosInsumos, 
    createTableProcesoFabricacion
};