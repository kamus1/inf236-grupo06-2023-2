const express = require('express');
const router = new express.Router();

const index = require('../controllers/index');
const tiendasController = require('../controllers/tiendasController');
const categoriasController = require('../controllers/categoriasController');
const tiendas_categoriasController = require('../controllers/tiendas_categoriasController');


// Esto es un ejemplo - SE PUEDE BORRAR 
router.get('/', (req, res) => res.json({message: 'Probando... La prueba fue un éxito API_TIENDAS! :P'}));

//==========================endpoints(Routes)============================//
// Esto es un ejemplo para endpoints
router.get('/createTableTiendas', index.createTableTiendas);
router.get('/createTableCategorias', index.createTableCategorias);
router.get('/createTableTiendasCategorias', index.createTableTiendasCategorias);

// Routes Tiendas
router.post('/api/tienda', tiendasController.createTienda);
router.get('/api/tienda', tiendasController.getTienda);
router.get('/api/tienda/:id', tiendasController.getTiendaById);
router.get('/api/tiendas', tiendasController.getTiendaByName);
router.put('/api/tienda/:id', tiendasController.updateTiendaById);
router.delete('/api/tienda/:id', tiendasController.deleteTiendaById);

//Routes Categorias
router.post('/api/categoria', categoriasController.createCategoria);
router.get('/api/categorias', categoriasController.getCategorias);
router.get('/api/categoria/:id', categoriasController.getCategoriaById);
router.get('/api/categorias', categoriasController.getCategoriaByName);
router.put('/api/categoria/:id', categoriasController.updateCategoriaById);
router.delete('/api/categoria/:id', categoriasController.deleteCategoriaById);

//Routes TiendasCategorias
router.post('/api/tienda/categoria', tiendas_categoriasController.createTiendaCategoria);
router.get('/api/tienda/categorias', tiendas_categoriasController.getTiendasCategorias);
router.get('/api/tienda/categoria/:id', tiendas_categoriasController.getTiendaCategoriaById);
router.get('/api/tienda/:id_tienda/categoria', tiendas_categoriasController.getTiendaCategoriaByIdTienda);
router.put('/api/tienda/:id/categoria', tiendas_categoriasController.updateTiendaCategoriaById);
router.delete('/api/tienda/:id/categoria', tiendas_categoriasController.deleteTiendaCategoriaById);

module.exports = router;