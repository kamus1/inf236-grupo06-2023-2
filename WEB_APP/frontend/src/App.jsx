import { Routes, Route } from "react-router-dom";
import Inicio from "./pages/Gestion/Inicio";
import Insumos from "./pages/Gestion/Insumos";
import NewInsumo from "./pages/Gestion/NewInsumo";
import Productos from "./pages/Gestion/Productos";

import NewProducto from "./pages/Gestion/NewProducto"
import EnProceso from "./pages/Gestion/EnProceso";
import Terminados from "./pages/Gestion/Terminados"
import Tienda from "./pages/Tiendas/Tienda";
import Emprendimiento from "./pages/Emprendimientos/Emprendimientos";

import "./App.css";

function App() {
  return (
    <div className="Aplicacion">
      <Routes>
        <Route path="/" element={<Emprendimiento />} />
        <Route path="/tienda/:nombreTienda/:idTienda" element={<Tienda />} />
        <Route path="/tienda/:nombreTienda/:idTienda/gestion/" element={<Inicio />} />
        <Route path="/tienda/:nombreTienda/:idTienda/gestion/insumos" element={<Insumos />} />
        <Route path="/tienda/:nombreTienda/:idTienda/gestion/insumos/new-insumo" element={<NewInsumo />} />
        <Route path="/tienda/:nombreTienda/:idTienda/gestion/productos" element={<Productos />} />
        <Route path="/tienda/:nombreTienda/:idTienda/gestion/productos/new-producto" element={<NewProducto />} />
        <Route path="/tienda/:nombreTienda/:idTienda/gestion/productos-en-proceso" element={<EnProceso />} />
        <Route path="/tienda/:nombreTienda/:idTienda/gestion/productos-en-terminados" element={<Terminados />} />
      </Routes>
    </div>
  );
}

export default App;
