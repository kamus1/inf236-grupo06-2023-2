import "./SearchInsumo.css";
import { useState, useEffect } from "react";

export function SearchInsumo({ onSearch }) {
  const [searchTerm, setSearchTerm] = useState("");

  const handleSearch = () => {
    onSearch(searchTerm);
  };

  return (
    <div className="search-bar">
      <input
        type="text"
        placeholder="Buscar insumos"
        value={searchTerm}
        onChange={(e) => setSearchTerm(e.target.value)}
      />
      <button className="btn" onClick={handleSearch}>
        Buscar
      </button>
    </div>
  );
}