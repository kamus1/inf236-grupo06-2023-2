const database = require('../db');


//Esta tabla es solo un ejemplo para que puedan ver como se hacen querys a la bdd, después puede ser eliminada

const createProducto = async (req, res) => { 
    const {id_tienda, nombre, precio, descripcion, url_imagen} = req.body

        // Verificaciones de tipo de datos
        if (typeof id_tienda !== 'number' ||
        typeof nombre !== 'string' ||
        typeof precio !== 'number' ||
        typeof descripcion !== 'string' ||
        typeof url_imagen !== 'string') {
        return res.status(400).json({'message': 'Error: tipo de dato incorrecto'});
    }

    const query = `INSERT INTO productos(id_tienda, nombre, precio, descripcion, url_imagen) VALUES (${id_tienda}, "${nombre}", "${precio}", "${descripcion}", "${url_imagen}")`;

    try {
        database.query(query, (err, result) => {
            if (err) throw err;
            res.json({'message': "Elemento insertado con éxito!", "id_producto": result.insertId})
        });
    } catch (err) {
        res.json({'message': err})
    }
};

const getProducto = async (req, res) => {
    const query = 'Select * FROM productos WHERE del = 0';
    try {
        database.query(query, (err, result) => {
            if (err) throw err;
            res.json(result)
        })
    } catch (err) {
        res.json({'message': err})
    }
};

const getProductoById = async (req, res) => {
    const {id} = req.params
    const query = `Select * FROM productos WHERE id = ${id}`;

    try {
        database.query(query, (err, result) => {
            if (err) throw err;
            res.json(result)
        })
    } catch (err) {
        res.json({'message': err})
    }
};

const getProductoByName = async (req, res) => {
    var nombre = req.query.nombre;
    let busqueda = nombre.replaceAll('-', ' ');

    const query = `Select * FROM productos WHERE nombre LIKE "%${busqueda}%"`;
    try {
        database.query(query, (err, result) => {
            if (err) throw err;
            res.json(result)
        })
    } catch (err) {
        res.json({'message': err})
    }
};

const getProductoByNameByTiendaId = async (req, res) => {
    const idTienda = req.params.id_tienda;
    var nombre = req.query.nombre;
    let busqueda = nombre.replaceAll('-', ' ');

    const query = `SELECT * FROM productos WHERE id_tienda = ${idTienda} AND nombre LIKE "%${busqueda}%"`;

    try {
        database.query(query, (err, result) => {
            if (err) throw err;
            res.json(result)
        })
    } catch (err) {
        res.json({'message': err})
    }
};


const updateProductoById = async (req, res) => {
    const {id} = req.params
    const {nombre, precio, descripcion, url_imagen, estado} = req.body

    const query = `UPDATE productos SET nombre = "${nombre}", precio = ${precio}, descripcion = "${descripcion}", url_imagen = "${url_imagen}", estado = ${estado} WHERE id = ${id}`;

    try {
        database.query(query, (err, result) => {
            if (err) throw err;
            res.json(result);
        })
    } catch (err) {
        res.json({'message': err})
    }
}

const deleteProductoById = async (req, res) => {
    const {id} = req.params

    const query =  `UPDATE productos SET del = 1 WHERE id = ${id}`;

    try {
        database.query(query, (err, result) => {
            if (err) throw err;
            res.json(result);
        })
    } catch (err) {
        res.json({'message': err})
    }
}

const getProductosByTiendaId = async (req, res) => {
    const {id} = req.params
    const query = `Select * FROM productos WHERE id_tienda = ${id} AND del = 0`;

    try {
        database.query(query, (err, result) => {
            if (err) throw err;
            res.json(result)
        })
    } catch (err) {
        res.json({'message': err})
    }
}


module.exports = {
    createProducto, 
    getProducto,
    getProductoById,
    getProductoByName,
    updateProductoById,
    deleteProductoById,
    getProductosByTiendaId,
    getProductoByNameByTiendaId
};