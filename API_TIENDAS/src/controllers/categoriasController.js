const database = require('../db');

//Categorias 
const createCategoria = async (req, res) => {
    const {nombre, descripcion} = req.body

    // Verificaciones de tipo de datos
    if (typeof nombre !== 'string' ||
    typeof descripcion !== 'string') {
    return res.status(400).json({'message': 'Error: tipo de dato incorrecto'});
    }

    const query = `INSERT INTO categorias(nombre, descripcion) VALUES ("${nombre}", "${descripcion}")`;

    try {
        database.query(query, (err, result) => {
            if (err) throw err;
            res.json({'message': "Elemento insertado con éxito!"})
        });
    } catch (err) {
        res.json({'message': err})
    }
};

const getCategorias = async (req, res) => {
    const query = 'Select * FROM categorias WHERE del = 0';
        try {
            database.query(query, (err, result) => {
                if (err) throw err;
                res.json(result)
            })
        } catch (err) {
            res.json({'message': err})
        }
};

const getCategoriaById = async (req, res) => {
    const {id} = req.params

    // Verificaciones de tipo de datos
    if (typeof id !== 'number'){
        return res.status(400).json({'message': 'Error: tipo de dato incorrecto'});
    }

    const query = `Select * FROM categorias WHERE id = ${id}`;

    try {
        database.query(query, (err, result) => {
            if (err) throw err;
            res.json(result)
        })
    } catch (err) {
        res.json({'message': err})
    }
};

const getCategoriaByName = async (req, res) => {
    var nombre = req.query.nombre

    // Verificaciones de tipo de datos
    if (typeof nombre !== 'string'){
        return res.status(400).json({'message': 'Error: tipo de dato incorrecto'});
        }

    let busqueda = nombre.replaceAll('-', ' ');
    const query = `Select * FROM categorias WHERE nombre LIKE "%${busqueda}%"`;

    try {
        database.query(query, (err, result) => {
            if (err) throw err;
            res.json(result)
        })
    } catch (err) {
        res.json({'message': err})
    }

}

const updateCategoriaById = async (req, res) => {
    const {id} = req.params
    const {nombre, descripcion} = req.body

    // Verificaciones de tipo de datos
    if (typeof id !== 'number' ||
    typeof nombre !== 'string' ||
    typeof descripcion !== 'string') {
    return res.status(400).json({'message': 'Error: tipo de dato incorrecto'});
    }

    const query = `UPDATE categorias SET nombre = "${nombre}", descripcion = "${descripcion}" WHERE id = ${id}`;

    try {
        database.query(query, (err, result) => {
            if (err) throw err;
            res.json(result);
        })
    } catch (err) {
        res.json({'message': err})
    }
}

const deleteCategoriaById = async (req, res) => {
    const {id} = req.params

    // Verificaciones de tipo de datos
    if (typeof id !== 'number') {
    return res.status(400).json({'message': 'Error: tipo de dato incorrecto'});
    }
    const query = `UPDATE categorias SET del = 1 WHERE id = ${id}`;

    try {
        database.query(query, (err, result) => {
            if (err) throw err;
            res.json(result);
        })
    } catch (err) {
        res.json({'message': err})
    }
}

module.exports = {
    createCategoria,
    getCategorias,
    getCategoriaById,
    getCategoriaByName,
    updateCategoriaById,
    deleteCategoriaById
};