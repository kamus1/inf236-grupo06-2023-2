const database = require('../db');
 
const createFabricacion = async (req, res) => {
    const {id_tienda, id_producto, date_inicio} = req.body
    const query = `INSERT INTO fabricacion (id_tienda, id_producto, date_inicio) VALUES (${id_tienda}, ${id_producto}, '${date_inicio}}')`;

    try {
        database.query(query, (err) => {
            if (err) throw err;
            res.json({'message': "Elemento insertado con éxito!"})
        });
    } catch (err) {
        res.json({'message': err})
    }
};

const getFabricacionAll = async (req, res) => {

    const query = 'Select fabricacion.id, productos.nombre, fabricacion.date_inicio, fabricacion.date_termino, fabricacion.terminado  FROM fabricacion INNER JOIN productos ON fabricacion.id_producto = productos.id WHERE fabricacion.del = 0';

    try {
        database.query(query, (err, result) => {
            if (err) throw err;
            res.json(result)
        })
    } catch (err) {
        res.json({'message': err})
    }
};

const getFabricacionById = async (req, res) => {

    const {id} = req.params
    
    const query = `Select fabricacion.id, productos.nombre, fabricacion.date_inicio, fabricacion.date_termino FROM fabricacion INNER JOIN productos ON fabricacion.id_producto = productos.id WHERE fabricacion.id = ${id}`;

    try {
        database.query(query, (err, result) => {
            if (err) throw err;
            res.json(result)
        })
    } catch (err) {
        res.json({'message': err})
    }
};

const getFabricacionIniciados = async (req, res) => {

    const query = 'Select fabricacion.id, productos.nombre, fabricacion.date_inicio, fabricacion.date_termino FROM fabricacion INNER JOIN productos ON fabricacion.id_producto = productos.id WHERE fabricacion.del = 0 AND fabricacion.terminado=0';

    try {
        database.query(query, (err, result) => {
            if (err) throw err;
            res.json(result)
        })
    } catch (err) {
        res.json({'message': err})
    }
};

const getFabricacionTerminados = async (req, res) => {

    const query = 'Select fabricacion.id, productos.nombre, fabricacion.date_inicio, fabricacion.date_termino FROM fabricacion INNER JOIN productos ON fabricacion.id_producto = productos.id WHERE fabricacion.del = 0 AND fabricacion.terminado=1';

    try {
        database.query(query, (err, result) => {
            if (err) throw err;
            res.json(result)
        })
    } catch (err) {
        res.json({'message': err})
    }
};

const updateFabricacionById = (req, res) => {
    const {id} = req.params
    const {id_producto, date_inicio, date_termino, terminado} = req.body

    const query = `UPDATE fabricacion SET id_producto = ${id_producto}, date_inicio = '${date_inicio}', date_termino = '${date_termino}', terminado = ${terminado} WHERE id = ${id}`;

    try {
        database.query(query, (err, result) => {
            if (err) throw err;
            res.json(result)
        })
        
    } catch (err) {
        res.json({'message': err})
    }
};

const deleteFabricacionById= async (req, res) => {
    const {id} = req.params

    const query =  `UPDATE fabricacion SET del = 1 WHERE id = ${id}`;

    try {
        database.query(query, (err, result) => {
            if (err) throw err;
            res.json(result);
        })
    } catch (err) {
        res.json({'message': err})
    }
};

const getFabricacionByTiendaId = async (req, res) => {
    const { id } = req.params;
    const query = `
        SELECT fabricacion.id, fabricacion.id_tienda, productos.nombre AS nombre_producto, fabricacion.date_inicio, fabricacion.date_termino, fabricacion.terminado
        FROM fabricacion 
        INNER JOIN productos ON fabricacion.id_producto = productos.id 
        WHERE fabricacion.id_tienda = ${id} AND fabricacion.del = 0
    `;

    try {
        database.query(query, (err, result) => {
            if (err) throw err;
            res.json(result);
        });
    } catch (err) {
        res.json({ 'message': err });
    }
};

const getFabricacionIniciadosByTiendaId = async (req, res) => {
    const { id } = req.params;
    const query = `
        SELECT fabricacion.id, fabricacion.id_tienda, productos.nombre AS nombre_producto, fabricacion.date_inicio, fabricacion.date_termino 
        FROM fabricacion  
        INNER JOIN productos ON fabricacion.id_producto = productos.id 
        WHERE fabricacion.id_tienda = ${id} AND fabricacion.del = 0 AND fabricacion.terminado = 0
    `;

    try {
        database.query(query, (err, result) => {
            if (err) throw err;
            res.json(result);
        });
    } catch (err) {
        res.json({ 'message': err });
    }
};

const getFabricacionTerminadosByTiendaId = async (req, res) => {
    const { id } = req.params;
    const query = `
        SELECT fabricacion.id, productos.nombre AS nombre_producto, fabricacion.date_inicio, fabricacion.date_termino 
        FROM fabricacion 
        INNER JOIN productos ON fabricacion.id_producto = productos.id 
        WHERE fabricacion.id_tienda = ${id} AND fabricacion.del = 0 AND fabricacion.terminado = 1
    `;

    try {
        database.query(query, (err, result) => {
            if (err) throw err;
            res.json(result);
        });
    } catch (err) {
        res.json({ 'message': err });
    }
};



module.exports = {
    createFabricacion,
    getFabricacionAll,
    getFabricacionById,
    getFabricacionIniciados,
    getFabricacionTerminados,
    updateFabricacionById,
    deleteFabricacionById,
    getFabricacionByTiendaId,
    getFabricacionIniciadosByTiendaId,
    getFabricacionTerminadosByTiendaId
};