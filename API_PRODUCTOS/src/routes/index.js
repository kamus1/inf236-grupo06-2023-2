const express = require('express');
const router = new express.Router();

const index = require('../controllers/index');
const productosController = require('../controllers/productosController');
const productosmaterialController = require('../controllers/productosmaterialController');
const fabricacionController = require('../controllers/fabricacionController');

// Esto es un ejemplo - SE PUEDE BORRAR
router.get('/', (req, res) => res.json({message: 'Probando... La prueba fue un éxito API_PRODUCTOS! :P'}));

//==========================endpoints(Routes)============================//
// Esto es un ejemplo para endpoints  
router.get('/createTableProducto', index.createTableProductos);
router.get('/createTableProductosInsumos', index.createTableProductosInsumos);
router.get('/createTableProcesoFabricacion', index.createTableProcesoFabricacion);

// Routes Productos
router.post('/api/catalogo', productosController.createProducto);
router.get('/api/catalogo', productosController.getProducto);
router.get('/api/catalogo/producto/:id', productosController.getProductoById);
router.get('/api/catalogo/producto', productosController.getProductoByName);
router.put('/api/catalogo/producto/:id', productosController.updateProductoById);
router.get('/api/catalogo/producto/tienda/:id', productosController.getProductosByTiendaId);
router.delete('/api/catalogo/producto/:id', productosController.deleteProductoById);

router.get('/api/catalogo/producto/tiendas/:id_tienda/', productosController.getProductoByNameByTiendaId); //1 query

//Routes Productos-Insumos  
router.post('/api/catalogo/producto/insumos/material', productosmaterialController.createProductoMaterial);
router.get('/api/catalogo/producto/insumos/material', productosmaterialController.getProductoMaterial);
router.get('/api/catalogo/producto/:id/material', productosmaterialController.getProductoMaterialesById);
router.put('/api/catalogo/producto/:id/material', productosmaterialController.updateProductoMaterialById);
router.delete('/api/catalogo/producto/:id/material', productosmaterialController.deleteProductoMaterialById);
router.get('/api/catalogo/producto/insumos/material/tienda/:id', productosmaterialController.getProductoMaterialByTiendaId);

//Routes Fabricacion
router.post('/api/fabricacion', fabricacionController.createFabricacion);
router.get('/api/fabricacion', fabricacionController.getFabricacionAll);
router.get('/api/fabricacion/proceso/:id', fabricacionController.getFabricacionById);
router.get('/api/fabricacion/activos', fabricacionController.getFabricacionIniciados);
router.get('/api/fabricacion/terminados', fabricacionController.getFabricacionTerminados);
router.put('/api/fabricacion/:id', fabricacionController.updateFabricacionById);
router.delete('/api/fabricacion/:id', fabricacionController.deleteFabricacionById);
router.get('/api/fabricacion/tienda/:id', fabricacionController.getFabricacionByTiendaId);

router.get('/api/fabricacion/activos/tienda/:id', fabricacionController.getFabricacionIniciadosByTiendaId);
router.get('/api/fabricacion/terminados/tienda/:id', fabricacionController.getFabricacionTerminadosByTiendaId);


module.exports = router;