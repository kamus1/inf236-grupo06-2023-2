const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const app = express();

app.use(cors());
app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

require('dotenv').config();

const routes = require('./src/routes/index');

app.use(morgan('dev'));
app.use(express.json());
app.use(routes);


app.listen(process.env.PORT_API, () => {
    console.log('Server running API_PRODUCTOS! Levantamiento exitoso!');
});