-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: BD06_STOCK
-- ------------------------------------------------------
-- Server version	8.3.0

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `insumos`
--

DROP TABLE IF EXISTS `insumos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `insumos` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_tienda` int DEFAULT NULL,
  `nombre` varchar(50) NOT NULL,
  `material` varchar(50) NOT NULL,
  `detalle` tinytext,
  `n_disponible` int DEFAULT NULL,
  `n_reservado` int DEFAULT '0',
  `n_usado` int DEFAULT '0',
  `del` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `insumos`
--

LOCK TABLES `insumos` WRITE;
/*!40000 ALTER TABLE `insumos` DISABLE KEYS */;
INSERT INTO `insumos` VALUES (1,1,'cartulina roja 30x30cm','papel','cartulina roja proarte',10,0,0,0),(2,1,'cartulina verde 20x10cm','papel','cartulina verde artel',4,0,0,0),(3,1,'cartulina rosa 50x50cm','papel','cartulina rosa artel',12,0,0,0),(4,1,'cartulina amarilla 50x50cm','papel','cartulina amarilla artel',5,0,0,0),(5,1,'lana blanca 100grs','hilos','lana blanca nako',3,0,0,0),(6,1,'lana roja 200grs','hilos','lana roja nako',4,0,0,0),(7,1,'lana café 200grs','hilos','lana café nako',5,0,0,0),(8,1,'colafría 225grs','pegamento','colafria escolar torre 225 grs',2,0,0,0),(9,1,'colafría 500grs','pegamento','colafria para madera 225 grs',2,0,0,0),(10,1,'silicona líquida 250ml','pegamento','silicona líquida artel',4,0,0,0),(11,1,'barra de silicona 12mm-30cm','pegamento','barras de silicona artel',30,0,0,0),(12,1,'goma eva glitter azul 30x30cm','goma eva','goma eva glitter azul artel',7,0,0,0),(13,1,'goma eva glitter rosa 30x30cm','goma eva','goma eva glitter rosa artel',4,0,0,0),(14,1,'goma eva verde 30x30cm','goma eva','goma eva verde proarte',10,0,0,0),(15,1,'goma eva blanca 30x30cm','goma eva','goma eva blanca proarte',10,0,0,0),(16,1,'goma eva negra 30x30cm','goma eva','goma eva negra proarte',10,0,0,0),(17,1,'goma eva glitter verde 30x30cm','goma eva','goma eva brillante verde proarte',10,0,0,0),(18,1,'goma eva glitter plateada 30x30cm','goma eva','goma eva brillante plateada proarte',10,0,0,0),(19,1,'cartulina blanca 30x30cm','papel','cartulina blanca proarte',4,0,0,0),(20,1,'cartulina negra 10x20cm','papel','cartulina negra proarte',5,0,0,0);
/*!40000 ALTER TABLE `insumos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'BD06_STOCK'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-05-01 22:12:14
