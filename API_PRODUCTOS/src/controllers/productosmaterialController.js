const database = require('../db');

const createProductoMaterial = async (req, res) => {
    const {id_tienda, id_producto, material, cantidad} = req.body
    const query = `INSERT INTO productos_insumos (id_tienda, id_producto, material, cantidad) VALUES (${id_tienda}, ${id_producto}, "${material}", ${cantidad})`;

    try {
        database.query(query, (err) => {
            if (err) throw err;
            res.json({'message': "Elemento insertado con éxito!"})
        });
    } catch (err) {
        res.json({'message': err})
    }
};

const getProductoMaterial = async (req, res) => {

    const query = 'Select * FROM productos_insumos WHERE del = 0';

    try {
        database.query(query, (err, result) => {
            if (err) throw err;
            res.json(result)
        })
    } catch (err) {
        res.json({'message': err})
    }
};

const getProductoMaterialesById = async (req, res) => {
    const {id} = req.params
    const query = `Select * FROM productos_insumos WHERE id_producto = ${id}`;

    try {
        database.query(query, (err, result) => {
            if (err) throw err;
            res.json(result)
        })
    } catch (err) {
        res.json({'message': err})
    }
};

const updateProductoMaterialById = async (req, res) => {
    const {id_producto} = req.params
    const {old_material, new_material} = req.body

    const query = `UPDATE productos_insumos SET material = "${new_material}" WHERE id_producto = ${id_producto} AND material = "${old_material}" `;

    try {
        database.query(query, (err, res) => {
            if (err) throw err;
            res.json(result);
        })
    } catch (err) {
        res.json({'message': err})
    }
}

const deleteProductoMaterialById = async (req, res) => {
    const {id_producto} = req.params
    const {material} = req.body

    const query =  `UPDATE productos SET del = 1 WHERE id_producto = ${id_producto} AND material = "${material}"`;

    try {
        database.query(query, (err, res) => {
            if (err) throw err;
            res.json(result);
        })
    } catch (err) {
        res.json({'message': err})
    }
}

const getProductoMaterialByTiendaId = async (req, res) => {
    const {id} = req.params
    const query = `Select * FROM productos_insumos WHERE id_tienda = ${id} AND del = 0`;

    try {
        database.query(query, (err, result) => {
            if (err) throw err;
            res.json(result)
        })
    } catch (err) {
        res.json({'message': err})
    }
}

module.exports = {
    createProductoMaterial, 
    getProductoMaterial,
    getProductoMaterialesById,
    updateProductoMaterialById,
    deleteProductoMaterialById,
    getProductoMaterialByTiendaId
};