import { Link } from "react-router-dom";
import {useParams} from "react-router-dom";

import { showStock } from "../../hooks/showStock";
import "./Productos.css"
import LoadingStock from "../../components/Insumos/LoadingStock"
import Modal from "../../components/Productos/Modal"
import React, { useState } from "react";
import { SearchInsumo } from "../../components/Insumos/SearchInsumo";

function Productos() {
  const { nombreTienda, idTienda} = useParams();

  const [searchTerm, setSearchTerm] = useState("");

  const handleSearch = (term) => {
    setSearchTerm(term);
    setInsumoId(null);
  };
  
  const apiUrl = searchTerm
    ? `http://localhost:8082/backend/productos/search/${idTienda}/${searchTerm.toLocaleLowerCase()}`
    : `http://localhost:8082/backend/productos/tienda/${idTienda}`;

  const { insumos, loading } = showStock(apiUrl);
  const [isModalOpen, setModalOpen] = useState(false);
  const [ProductoId, setProductoId] = useState(null);

  const handleOpenModal = (insumoId) => {
    setProductoId(insumoId);
    setModalOpen(true);
  };

  const handleCloseModal = () => {
    setProductoId(null);
    setModalOpen(false);
  };

  return (
    <div className="conteiner">
      
      <section className="white-section overflowy">
        <Modal idproducto={ProductoId} idtienda={idTienda} nombretienda = {nombreTienda} isOpen={isModalOpen} onClose={handleCloseModal}></Modal>
        <h1>Productos</h1>
        <article className="products-article">
        <main className="products">
          <ul>
          {loading && <LoadingStock />}
          {insumos?.map((producto) => {
            return (
              <li key={producto.id} onClick={() => handleOpenModal(producto.id)}>
                <img src={producto.url_imagen} alt={producto.nombre} />
                <div>
                  <strong>{producto.nombre}</strong> - ${producto.precio}
                </div>
              </li>
            )
          })}
          </ul>
        </main>
        </article>
        <hr className="green-hr"/>
      </section>
      <section className="red-section">
        <h2>{nombreTienda}</h2>
        <hr className="white-hr"/>
        <div className="link-container">
          <SearchInsumo onSearch={handleSearch}></SearchInsumo>
          <Link to={`/tienda/${nombreTienda}/${idTienda}/gestion/productos/new-producto`} className="btn">Agregar nuevo producto</Link>
          <Link to={`/tienda/${nombreTienda}/${idTienda}/gestion`} className="btn">
            Volver al Menú Principal
        </Link>
        </div>
      </section>
    </div>
  );
}

export default Productos;
