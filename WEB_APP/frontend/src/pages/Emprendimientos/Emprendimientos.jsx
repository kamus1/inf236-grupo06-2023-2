import { useEffect, useState } from "react";
import EmprendimientosCard from "../../components/Emprendimientos/EmprendimientosCard";
import "./Emprendimiento.css"


function Emprendimiento() {

    const [tiendas, setTiendas] = useState([]);

    useEffect(() => {
        fetch('http://localhost:8082/backend/tiendas')
          .then(response => response.json())
          .then(data => setTiendas(data))
          .catch(error => console.error('Error al obtener productos:', error));
    
      }, []);

    return (
        <div className="emprendimientos-page">
            <div className="emprendimientos-hero">
                <h1>Descubre el Mundo de la Innovación <span>Emprendedora</span></h1>
                <p>Bienvenido a un espacio dedicado a la creatividad, la pasión y la visión empresarial: nuestra plataforma es el epicentro de los emprendimientos más lindos del momento.</p>
            </div>
            <section className="conoce-nuestras-tiendas">
                <h2>¡Conoce nuestras <span>tiendas</span>!</h2>
                <p>Échale un vistado a todo lo que tenemos para ofrecer</p>
                <section className="emprendimientos-section">
                    {
                        tiendas.map((tienda) => (
                            <EmprendimientosCard key={tienda.id} emprendimiento={tienda}/>
                        ))
                    }
                </section>
            </section>
            
            <footer>
                <h4>Explora, inspira y <span>emprende</span></h4>
                <p>⭐</p>
            </footer>
        </div>
    );
} export default Emprendimiento;