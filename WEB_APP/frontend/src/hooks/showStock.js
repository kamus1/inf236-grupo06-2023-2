import { useState, useEffect } from "react";

export function showStock(url) {
  const [insumos, setInsumos] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    fetch(url)
      .then((response) => response.json())
      .then((data) => setInsumos(data))
      .finally(() => setLoading(false));
  }, [url]);

  return { insumos, loading };
}
