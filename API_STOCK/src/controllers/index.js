const database = require('../db');

const createTableStock = (req, res) => {
    const query =  'CREATE TABLE IF NOT EXISTS insumos(id INT AUTO_INCREMENT, id_tienda INT, nombre VARCHAR(50) NOT NULL, material VARCHAR(50) NOT NULL, detalle TINYTEXT, n_disponible INT, n_reservado INT DEFAULT 0, n_usado INT DEFAULT 0, del BOOL DEFAULT 0, PRIMARY KEY(id))';

    database.query(query, (err) => {
        if (err) throw err;
        res.send('Tabla creada')
    });
};


module.exports = {
    createTableStock
};