import "./Tienda.css"

import ProductosCard from "../../components/Tienda/ProductosCard";
import { useEffect, useState } from "react";
import {useParams} from "react-router-dom";
import { Link } from "react-router-dom";

function Tienda() {
  const { nombreTienda, idTienda } = useParams();
  const [tienda, setTienda] = useState("");	
  const [productos, setProductos] = useState("");	

  useEffect(() => {
    const fetchData = async () => {
      try {
        const [tiendaResponse, productosResponse] = await Promise.all([
          fetch(`http://localhost:8082/backend/tiendas/${idTienda}`),
          fetch(`http://localhost:8082/backend/productos/tienda/${idTienda}`)
        ]);

        if (!tiendaResponse.ok || !productosResponse.ok) {
          throw new Error('Error al obtener datos de la tienda o productos');
        }

        const tiendaData = await tiendaResponse.json();
        const productosData = await productosResponse.json();

        setTienda(tiendaData[0]);
        setProductos(productosData);
      } catch (error) {
        console.error('Error:', error);
      }
    };

    fetchData();
  }, [idTienda]);

  return (
    <div className="tienda-box">
      <header>
        <Link to="/" className="link-descubre">¡Descubre más emprendimientos acá!</Link>
        <p>⭐</p>
      </header>
      <nav>
        <div className="imagen-redonda-container">
              <img src={`/logos-emprendimientos/${nombreTienda}.webp`} alt={nombreTienda} className="imagen-redonda" />
        </div>
        <div className="links">
          <a href={tienda.url_redes} target="_blank" className="contact-me">¿Quieres hablar?</a>
          <a href={tienda.url_redes} target="_blank">
            <svg  xmlns="http://www.w3.org/2000/svg"  width="24"  height="24"  viewBox="0 0 24 24"  fill="none"  stroke="currentColor"  strokeWidth="1.5"  strokeLinecap="round"  strokeLinejoin="round"  className="icon icon-tabler icons-tabler-outline icon-tabler-brand-instagram"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M4 4m0 4a4 4 0 0 1 4 -4h8a4 4 0 0 1 4 4v8a4 4 0 0 1 -4 4h-8a4 4 0 0 1 -4 -4z" /><path d="M12 12m-3 0a3 3 0 1 0 6 0a3 3 0 1 0 -6 0" /><path d="M16.5 7.5l0 .01" /></svg>
          </a>
        </div>
      </nav>
      <section className="hero-section-emprendimientos" >
        <div className="contenedor-hero-image" style={{backgroundImage: `url('/hero-emprendimientos/Hero-${nombreTienda}.webp')`}} >
          	<div>
              <h1>{nombreTienda}</h1>
              <p>{tienda.descripcion}</p>
            </div>
        </div>
      </section>
      
      <section>
        <div className="contener-title-productos">
          <h2>PRODUCTOS</h2>
          <p>Lo más destacados ⭐</p>
          </div>
          <section className="productos-section-tienda">
          {productos && productos.map((producto) => {
            if (producto.estado === 1) {
                return (
                    <ProductosCard
                        key={producto.id}
                        img={producto.url_imagen}
                        desc={producto.descripcion}
                        name={producto.nombre}
                        price={producto.precio}
                    />
                );
            }
          })}
        </section>
      </section>

      <footer className="footer-tienda">
        <p>{nombreTienda}, {tienda.descripcion}</p>
        <Link to={`/tienda/${nombreTienda}/${idTienda}/gestion`} className="link-descubre">Gestionar Tienda</Link>
      </footer>
      
      
      
    </div>
  );
}

export default Tienda;

