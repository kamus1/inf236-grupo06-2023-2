import { Link } from "react-router-dom";
import {useParams} from "react-router-dom";

import { useState } from "react";
import "./NewProducto.css";
import { showStock } from "../../hooks/showStock";

function NewProducto() {
  const { nombreTienda, idTienda} = useParams();
  const { insumos , loading } = showStock(`http://localhost:8082/backend/insumos/tienda/${idTienda}`);

  const [isSubmitting, setIsSubmitting] = useState(false);
  const [successMessage, setSuccessMessage] = useState("");
  const [formData, setFormData] = useState({
    nombre: "",
    precio: "",
    descripcion: "",
    foto: "",
    insumosSeleccionados: [],
  });


  const handleSubmit = (e) => {
  e.preventDefault();
  const isQuantityValid = !isNaN(Number(formData.precio));

  if (!isQuantityValid) {
    setIsSubmitting(false);
    setSuccessMessage("Coloque un precio válido");
    return;
  }

  setIsSubmitting(true);
  setSuccessMessage("Producto agregado con éxito");
  setFormData((prevData) => ({
    ...prevData,
    insumosSeleccionados: prevData.insumosSeleccionados.filter((item) => item.cantidad > 0),
  }));

  fetch('http://localhost:8082/backend/productos',{
            method:"POST",
            headers: {
              'Content-Type': 'application/json',
            },
            body:JSON.stringify(
                {
                    id_tienda: parseInt(idTienda),
                    nombre: formData.nombre,
                    precio: formData.precio,
                    descripcion: formData.descripcion,
                    url_imagen: formData.foto,
                    insumosSeleccionados: formData.insumosSeleccionados,
                }
            )
        })
            .then(res=>res.json())
            .then(json=>console.log(json))
};

const handleChange = (event) => {
  const { name, value } = event.target;

  if (name.startsWith("cantidad-")) {
    const insumoId = parseInt(name.replace("cantidad-", ""));
    const cantidad = parseInt(value);

    // Verifica si la cantidad es un número válido antes de agregar al estado
    if (!isNaN(cantidad)) {
      // Actualiza el estado con el nuevo objeto en insumosSeleccionados
      setFormData((prevData) => ({
        ...prevData,
        insumosSeleccionados: [
          ...prevData.insumosSeleccionados.filter((item) => item.id !== insumoId), // Elimina el objeto existente si ya está en la lista
          { id: insumoId, nombre: insumos.find((insumo) => insumo.id === insumoId).nombre, cantidad: cantidad },
        ],
      }));
    }

  } else if (name === "foto") {
    setFormData((prevData) => ({
      ...prevData,
      foto: value,
    }));
  } else if (name === "precio") {
    const precio = isNaN(Number(value)) ? "" : Number(value);

    setFormData((prevData) => ({
      ...prevData,
      precio: precio,
    }));
  } else {
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  }
};

  const handleClick = () => {
    setIsSubmitting(false);
    setSuccessMessage("");
    setFormData({
      nombre: "",
      precio: "",
      descripcion: "",
      foto: "",
      insumosSeleccionados: [],
    });

  };

  return (
    <div className="conteiner scroll-pag">
      <section className="white-section center">
        <h1>Agregar nuevo Producto</h1>
        <form method="post" onSubmit={handleSubmit}>
          <input
            type="text"
            name="nombre"
            placeholder="Nombre producto"
            onChange={handleChange}
            autoComplete="off"
            required
          />
          <input
            type="text"
            name="precio"
            placeholder="Precio"
            onChange={handleChange}
            autoComplete="off"
            required
          />
          <input
            type="text"
            name="descripcion"
            placeholder="Descripción"
            onChange={handleChange}
            autoComplete="off"
            required
          />
          <input
            type="text"
            name="foto"
            placeholder="Link de la imagen"
            onChange={handleChange}
            autoComplete="off"
            required
          />
          <p className="start">Coloque la cantidad del material a utilizar: </p>
          <div className="checkbox-container">
          
          {insumos?.map((insumo) => (
            <div key={insumo.id}>
              <label>
                <input
                  type="text"
                  name={`cantidad-${insumo.id}`}
                  placeholder="Cantidad"
                  onChange={handleChange}
                  autoComplete="off"          
                />
                <span>{insumo.nombre}</span>
              </label>
            </div>
          ))}
          </div>

          <button
            type="submit"
            className={`newinsumo-btn ${
              isSubmitting ? "hidden" : "visible-inline"
            }`}
            disabled={isSubmitting}
          >
            Agregar producto
          </button>
          <button type="reset" className={`newinsumo-btn ${
              isSubmitting ? "visible-inline" : "hidden"
            }`} onClick={handleClick}>
            Agregar nuevo producto
          </button>
          {successMessage && (
            <p className="success-message">{successMessage}</p>
          )}
        </form>
        
        <hr className="green-hr"/>
      </section>
      <section className="red-section">
        <h2>{nombreTienda}</h2>
        <hr className="white-hr" />
       <div className="link-conteiner">
          <Link to={`/tienda/${nombreTienda}/${idTienda}/gestion/productos`} className="btn">Haz clic volver a producto</Link>
       </div>
      </section>
    </div>
  );
}

export default NewProducto;
