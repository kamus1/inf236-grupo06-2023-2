const database = require('../db');


//Esta tabla es solo un ejemplo para  que puedan ver como se hacen querys a la bdd, después puede ser eliminada

const createTienda = async (req, res) => {
    const {nombre, descripcion, url_logo, url_redes} = req.body

    // Verificaciones de tipo de datos
    if (typeof nombre !== 'string' ||
    typeof descripcion !== 'string' ||
    typeof url_logo !== 'string' ||
    typeof url_redes !== 'string') {
    return res.status(400).json({'message': 'Error: tipo de dato incorrecto'});
    }

    const query = `INSERT INTO tiendas(nombre, descripcion, url_logo, url_redes) VALUES ("${nombre}", "${descripcion}", "${url_logo}", "${url_redes}")`;

    try {
        database.query(query, (err, result) => {
            if (err) throw err;
            res.json({'message': "Elemento insertado con éxito!", "id_tienda": result.insertId})
        });
    } catch (err) {
        res.json({'message': err})
    }
};

const getTienda = async (req, res) => {
    const query = 'Select * FROM tiendas WHERE del = 0';
        try {
            database.query(query, (err, result) => {
                if (err) throw err;
                res.json(result)
            })
        } catch (err) {
            res.json({'message': err})
        }
};

const getTiendaById = async (req, res) => {
    const {id} = req.params

    const query = `Select * FROM tiendas WHERE id = ${id}`;

    try {
        database.query(query, (err, result) => {
            if (err) throw err;
            res.json(result)
        })
    } catch (err) {
        res.json({'message': err})
    }
};

const getTiendaByName = async (req, res) => {
    var nombre = req.query.nombre

    // Verificaciones de tipo de datos
    if (typeof nombre !== 'string'){
        return res.status(400).json({'message': 'Error: tipo de dato incorrecto'});
        }

    let busqueda = nombre.replaceAll('-', ' ');
    const query = `Select * FROM tiendas WHERE nombre LIKE "%${busqueda}%"`;

    try {
        database.query(query, (err, result) => {
            if (err) throw err;
            res.json(result)
        })
    } catch (err) {
        res.json({'message': err})
    }

}

const updateTiendaById = async (req, res) => {
    const {id} = req.params
    const {nombre, descripcion, url_logo, url_redes} = req.body

    // Verificaciones de tipo de datos
    if (typeof id !== 'number' ||
    typeof nombre !== 'string' ||
    typeof descripcion !== 'string' ||
    typeof url_logo !== 'string' ||
    typeof url_redes !== 'string') {
    return res.status(400).json({'message': 'Error: tipo de dato incorrecto'});
    }

    const query = `UPDATE tiendas SET nombre = "${nombre}", descripcion = "${descripcion}", url_logo = "${url_logo}", url_redes = "${url_redes}" WHERE id = ${id}`;

    try {
        database.query(query, (err, result) => {
            if (err) throw err;
            res.json(result);
        })
    } catch (err) {
        res.json({'message': err})
    }
}

const deleteTiendaById = async (req, res) => {
    const {id} = req.params

    // Verificaciones de tipo de datos
    if (typeof id !== 'number') {
    return res.status(400).json({'message': 'Error: tipo de dato incorrecto'});
    }
    const query = `UPDATE tiendas SET del = 1 WHERE id = ${id}`;

    try {
        database.query(query, (err, result) => {
            if (err) throw err;
            res.json(result);
        })
    } catch (err) {
        res.json({'message': err})
    }
}


module.exports = {
    createTienda,
    getTienda,
    getTiendaById,
    getTiendaByName,
    updateTiendaById,
    deleteTiendaById
};
