import LoadingStock from "../../components/Insumos/LoadingStock";
import { showStock } from "../../hooks/showStock";
import "./Terminados.css"
import "./EnProceso.css"
import { Link } from "react-router-dom";
import {useParams} from "react-router-dom";

function EnProceso() {
    const { nombreTienda, idTienda} = useParams();
    const { insumos , loading } = showStock(`http://localhost:8082/backend/fabricacion/tienda/${idTienda}`);
    const formatDate = (dateString) => {
        const fechaFormateada = new Date(dateString);
        const dia = fechaFormateada.getDate();
        const mes = fechaFormateada.getMonth() + 1;
        const año = fechaFormateada.getFullYear();
        return `${dia}/${mes}/${año}`;
    };

    return(
        <div className="conteiner">
            <section className="white-section">
                 <h1>Productos Terminados</h1>
                 
                 <article className="pp-section">
                    <div className="pt-container left">
                        <p>Id</p>
                        <p>Fecha de Termino</p>
                        <p className="start">Nombre Producto</p>
                        
                    </div>
                 <ul>
                    {loading && <LoadingStock />}
                    {insumos?.length > 0 && insumos
                        .filter(insumo => insumo.terminado === 1)
                        .map((insumo) => (
                        <li key={insumo.id}>
                            <div className="pt-container" >
                                <p>{insumo.id}</p>
                                <p>{formatDate(insumo.date_termino)}</p>
                                <p className="start">{insumo.nombre_producto}</p>
                            </div>
                        </li>
                        
                    ))}
                 </ul>
                 </article>
                 <hr className="green-hr"/>
    
            </section>
            <section className="red-section">
                <h2>{nombreTienda}</h2>
                <hr className="white-hr"/>
                <Link to={`/tienda/${nombreTienda}/${idTienda}/gestion`} className="btn">
                    Volver al Menú Principal
                </Link>
            </section>
        </div>
        
    )
}

export default EnProceso;