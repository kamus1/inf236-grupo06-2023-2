import "./EmprendimientosCard.css"
import { Link } from "react-router-dom";

function EmprendimientosCard({ emprendimiento }) {
  const { id, nombre, descripcion, url_logo } = emprendimiento;
  return (
    <Link to={`/tienda/${nombre}/${id}`} className="emprendimientos-card-section">
        <div className="imagen-redonda-container">
            <img src={url_logo} alt={nombre} className="imagen-redonda" />
        </div>
        <h3>{nombre}</h3>
        <p>{descripcion}</p>
    </Link>
    
  );
} export default EmprendimientosCard;
