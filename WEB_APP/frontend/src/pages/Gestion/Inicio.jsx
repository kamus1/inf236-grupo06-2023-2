import { Link } from "react-router-dom";
import {useParams} from "react-router-dom";
import { useEffect, useState } from "react";

import "./Inicio.css";

function Inicio() {
  const { nombreTienda, idTienda } = useParams();

  const [tienda, setTienda] = useState([]);

    useEffect(() => {
        fetch(`http://localhost:8082/backend/tiendas/${idTienda}`)
          .then(response => response.json())
          .then(data => setTienda(data[0]))
          .catch(error => console.error('Error al obtener productos:', error));
    
      }, []);

  return (
    <div className="box">
      <div className="conteiner-inicio">
        <section className="inicio-white">
          <div className="welcome-conteiner">
            <div className="imagen-redonda-container">
              <img src={tienda.url_logo} alt={nombreTienda} className="imagen-redonda" />
            </div>
            <h1>¡Hola, {nombreTienda}!</h1>
          </div>
        </section>
        <section className="inicio-red">
          <h2>{nombreTienda}</h2>
          <hr className="white-hr"/>

          <Link to={`/tienda/${nombreTienda}/${idTienda}/gestion/insumos`} className="btn">
            Administrar Insumos
          </Link> 
          <Link to={`/tienda/${nombreTienda}/${idTienda}/gestion/productos`} className="btn">
            Administrar Productos
          </Link> 
          <Link to={`/tienda/${nombreTienda}/${idTienda}/gestion/productos-en-proceso`} className="btn">
            Productos en Proceso
          </Link>
          <Link to={`/tienda/${nombreTienda}/${idTienda}/gestion/productos-en-terminados`} className="btn">
            Productos Terminados
          </Link>
          <Link to={`/tienda/${nombreTienda}/${idTienda}`} className="btn">
            Volver a la tienda
          </Link>
        </section>
      </div>
    </div>
  );
}

export default Inicio;
