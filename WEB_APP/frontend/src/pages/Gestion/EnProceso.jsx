import LoadingStock from "../../components/Insumos/LoadingStock";
import { showStock } from "../../hooks/showStock";
import "./EnProceso.css"
import { Link } from "react-router-dom";
import {useParams} from "react-router-dom";

import React, { useState, useEffect } from "react";

function EnProceso() {
    const { nombreTienda, idTienda} = useParams();
    const { insumos , loading } = showStock(`http://localhost:8082/backend/fabricacion/tienda/${idTienda}`);

    const formatDate = (dateString) => {
        const fechaFormateada = new Date(dateString);
        const dia = fechaFormateada.getDate();
        const mes = fechaFormateada.getMonth() + 1;
        const año = fechaFormateada.getFullYear();
        return `${dia}/${mes}/${año}`;
    };

    const updatePedido = (id) => {

        const currentDate = new Date();
        const dia = currentDate.getDate();
        const mes = currentDate.getMonth() + 1;
        const año = currentDate.getFullYear();

        const fecha = `${año}/${mes}/${dia}`;
        console.log(fecha)

        fetch(`http://localhost:8082/backend/fabricacion/${id}`,{
            method:"PUT",
            headers: {
              'Content-Type': 'application/json',
            },
            body:JSON.stringify(
                {
                    date_termino: fecha,
                }
            )
        })
            .then(res=>res.json())
            .then(json=>console.log(json))
            .then(() => window.location.reload());

    }


    const deletePedido = (id) => {
        fetch(`http://localhost:8082/backend/fabricacion/${id}`,{
            method:"DELETE"
        })
            .then(res=>res.json())
            .then(json=>console.log(json))
            .then(() => window.location.reload()); 
    };

    return(
        <div className="conteiner">
            <section className="white-section">
                 <h1>Productos en Proceso</h1>
                 
                 <article className="pp-section">
                    <div className="pp-container left">
                        <p>Terminado</p>
                        <p>Id</p>
                        <p>Fecha de Inicio</p>
                        <p className="start">Nombre Producto</p>
                        
                    </div>
                 <ul>
                    {loading && <LoadingStock />}
                    {insumos?.length > 0 && insumos
                        .filter(insumo => insumo.terminado === 0)
                        .map(insumo => (
                            <li key={insumo.id}>
                                <div className="pp-container">
                                    <button className="deletebtn" onClick={() => updatePedido(insumo.id)}>
                                        <svg xmlns="http://www.w3.org/2000/svg" className="icon icon-tabler icon-tabler-circle-check" width="20" height="20" viewBox="0 0 24 24" strokeWidth="1.5" stroke="#00899b" fill="none" strokeLinecap="round" strokeLinejoin="round">
                                            <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                            <path d="M12 12m-9 0a9 9 0 1 0 18 0a9 9 0 1 0 -18 0" />
                                            <path d="M9 12l2 2l4 -4" />
                                        </svg>
                                    </button>
                                    <p>{insumo.id}</p>
                                    <p>{formatDate(insumo.date_inicio)}</p>
                                    <p className="start">{insumo.nombre_producto}</p>
                                    <button className="deletebtn" onClick={() => deletePedido(insumo.id)}>
                                        <svg xmlns="http://www.w3.org/2000/svg" className="icon icon-tabler icon-tabler-trash" width="20" height="20" viewBox="0 0 24 24" strokeWidth="1.5" stroke="#00899b" fill="none" strokeLinecap="round" strokeLinejoin="round">
                                            <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                            <path d="M4 7l16 0" />
                                            <path d="M10 11l0 6" />
                                            <path d="M14 11l0 6" />
                                            <path d="M5 7l1 12a2 2 0 0 0 2 2h8a2 2 0 0 0 2 -2l1 -12" />
                                            <path d="M9 7v-3a1 1 0 0 1 1 -1h4a1 1 0 0 1 1 1v3" />
                                        </svg>
                                    </button>
                                </div>
                            </li>
                        ))}
                 </ul>
                 </article>
                 <hr className="green-hr"/>
    
            </section>
            <section className="red-section">
                <h2>{nombreTienda}</h2>
                <hr className="white-hr"/>
                <Link to={`/tienda/${nombreTienda}/${idTienda}/gestion`} className="btn">
            Volver al Menú Principal
          </Link>
            </section>
        </div>
        
    )
}

export default EnProceso;