import { Link } from "react-router-dom";
import {useParams} from "react-router-dom";
import { useState } from "react";
import { SearchInsumo } from "../../components/Insumos/SearchInsumo";
import { showStock } from "../../hooks/showStock";
import ModalInsumos from "../../components/Insumos/ModalInsumos";
import LoadingStock from "../../components/Insumos/LoadingStock";

import "./Insumos.css";

function Insumos() {
  const { nombreTienda, idTienda} = useParams();

  const [searchTerm, setSearchTerm] = useState("");
  const [insumoId, setInsumoId] = useState(null);
  const [isModalOpen, setModalOpen] = useState(false);

  const handleSearch = (term) => {
    setSearchTerm(term);
    setInsumoId(null);
  };

  const apiUrl = searchTerm
    ? `http://localhost:8082/backend/insumos/search/${idTienda}/${searchTerm.toLocaleLowerCase()}`
    : `http://localhost:8082/backend/insumos/tienda/${idTienda}`;

  const { insumos, loading } = showStock(apiUrl);
    

  const handleOpenModal = (insumoId) => {
    setInsumoId(insumoId);
    setModalOpen(true);
  };

  const handleCloseModal = () => {
    setInsumoId(null);
    setModalOpen(false);
  };

  const resetId = () => {
    showB(false);
  };

  return (
    <div className="conteiner">
      <section className="white-section">
        <ModalInsumos idinsumo={insumoId} isOpen={isModalOpen} onClose={handleCloseModal}></ModalInsumos>
        <h1>Insumos</h1>
        <header className="header">
          <div className="insumo-header">Insumo</div>
          <div className="cantidad-general-header">
            <div className="cantidad-header">Cantidad en Stock</div>
            <div className="cantidad-disponible">Cantidad Disponible</div>
            <div className="cantidad-reservada">Cantidad Reservada</div>
          </div>
        </header>
        <article>
          {loading && <LoadingStock />}
          {insumos?.length > 0 && insumos
            .map((insumo) => (
            <div className="insumos-container" key={insumo.id}>
              <p className="insumo" onClick={() => handleOpenModal(insumo.id)}>
                {insumo.nombre}
              </p>
              <p className="cantidad">{insumo.n_disponible}</p>
              <p className="cantidad">{insumo.n_reservado}</p>
            </div>
          ))}
        </article>
        <hr className="green-hr"/>
      </section>
      <section className="red-section">
        <h2>{nombreTienda}</h2>
        <hr className="white-hr"/>
        <SearchInsumo onSearch={handleSearch}></SearchInsumo>
        <Link to="new-insumo" className="btn" onClick={resetId}>
            Agregar nuevo insumo
        </Link>
        <Link to={`/tienda/${nombreTienda}/${idTienda}/gestion`} className="btn" onClick={resetId}>
            Volver al Menú Principal
        </Link>
      </section>
    </div>
  );
}

export default Insumos;
