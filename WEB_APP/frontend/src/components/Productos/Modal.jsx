import React, { useState, useEffect } from "react";
import "./Modal.css";
import { showStock } from "../../hooks/showStock";

function Modal({ idproducto, idtienda, nombretienda, isOpen, onClose}) {
  const [formData, setFormData] = useState({
    nombre: "",
    precio: "",
    descripcion: "",
    url_picture: "",
    estado: ""
  });

  const [isSubmitting, setIsSubmitting] = useState(false);
  const [successMessage, setSuccessMessage] = useState("");

  //Esto es para autocompletar los apartados con la info inicial
  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch(`http://localhost:8082/backend/productos/${idproducto}`);
        const producto = await response.json();

        setFormData({
          nombre: producto[0].producto.nombre,
          precio: producto[0].producto.precio,
          descripcion: producto[0].producto.descripcion,
          url_picture: producto[0].producto.url_imagen,
          estado: producto[0].producto.estado,
        });
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    if (isOpen) {
      fetchData();
    }
  }, [idproducto, isOpen]);

  //Encontramos los insumos que se utilizan en el producto
  const { insumos, loading } = showStock(`http://localhost:8082/backend/productos/${idproducto}`);

  const handleChange = (event) => {
  const { name, value } = event.target;

  setFormData((prevData) => ({
    ...prevData,
    [name] : value,
  }));
};

  const handleClickMirar = () => {
  setFormData((prevData) => ({
    ...prevData,
    estado: prevData.estado? 0:1,
  }));
};
  
  const handleSubmit = (e) => {
    e.preventDefault();

    const isPriceValid = !isNaN(Number(formData.precio));

    if (!isPriceValid) {
      setIsSubmitting(false);
      setSuccessMessage("Coloque un precio válido");
      return;
    }

    setIsSubmitting(true);
    setSuccessMessage("Producto actualizado con éxito");

    fetch(`http://localhost:8081/api/catalogo/producto/${idproducto}`,{
            method:"PUT",
            headers: {
              'Content-Type': 'application/json',
            },
            body:JSON.stringify(
                {
                    nombre: formData.nombre,
                    precio: parseInt(formData.precio, 10) || 0,
                    descripcion: formData.descripcion,
                    url_imagen: formData.url_picture,
                    estado: formData.estado,
                }
            )
        })
            .then(res=>res.json())
            .then(json=>console.log(json))
            .then(() => window.location.reload());
  };

const handleFabricar = () => {

  const currentDate = new Date();
  const dia = currentDate.getDate();
  const mes = currentDate.getMonth() + 1;
  const año = currentDate.getFullYear();

  const fecha = `${año}/${mes}/${dia}`;

  fetch(`http://localhost:8082/backend/productos/${idproducto}/fabricar`,{
            method:"POST",
            headers: {
              'Content-Type': 'application/json',
            },
            body:JSON.stringify(
                {
                    date_inicio: fecha,
                    id_tienda: idtienda,
                }
            )
        })
            .then(res=>res.json())
            .then(json=>console.log(json))
            .then(() => window.location.reload());
}

const handleDelete = () => {
  fetch(`http://localhost:8081/api/catalogo/producto/${idproducto}`,{
    method:"DELETE"
  })
  .then(res=>res.json())
  .then(json=>console.log(json))  
  .then(() => window.location.reload());
   
}

const handleClick = () => {
  onClose();
  setIsSubmitting(false);
  setSuccessMessage("");
  setFormData({
      nombre: "",
      precio: "",
      descripcion: "",
      url_picture: "",
      estado: "",
  });
};

  return (
    isOpen && (
      <div className="modal-overlay">
        <div className="modal">
          <section className="act-zone overflowy">
            <h1>Actualizar Producto</h1>
            <form method="post" onSubmit={handleSubmit}>
              <input
                type="text"
                name="nombre"
                placeholder="Nombre producto"
                onChange={handleChange}
                autoComplete="off"
                defaultValue={formData.nombre}
              />
              <input
                type="text"
                name="precio"
                placeholder="Precio"
                onChange={handleChange}
                autoComplete="off"
                defaultValue={formData.precio}
              />

              <input
                type="text"
                name="descripcion"
                placeholder="Descripción"
                onChange={handleChange}
                autoComplete="off"
                defaultValue={formData.descripcion}
              />

              <input
                type="text"
                name="url_picture"
                placeholder="Link de la imagen"
                onChange={handleChange}
                autoComplete="off"
                defaultValue={formData.url_picture}
              />
              <button
                type="button"
                onClick={handleClickMirar}
                className={"actpro-btn"}
              >
                {formData.estado === 1 && "En muestra"}
                {formData.estado === 0 && "Discontinuado"}
              </button>
              <button
                type="submit"
                className={"newinsumo-btn"}
                disabled={isSubmitting}
              >
                Actualizar Producto
              </button>
              {successMessage && (
                <p className="success-message">{successMessage}</p>
              )}
              
            </form>
          </section>
          <section className="buttons-zone">
            <h2>{nombretienda}</h2>
            <hr />
            <h3>Insumos utilizados:</h3>
            <ul>
            {insumos[0].materiales.map((material) => (
              <li key={material.id}>{material.material}</li>
            ))}
            </ul>
            <hr />
            <div className="contenedor-btn">
              <button className="btn-red" onClick={handleFabricar}>Fabricar</button>
              <button className="btn-red" onClick={handleDelete}>Borrar</button>
            </div>
            <button className="btn-border-red" onClick={handleClick}>
              Cerrar
            </button>
          </section>
        </div>
      </div>
    )
  );
}

export default Modal;
