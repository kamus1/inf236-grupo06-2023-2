-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: BD06_PRODUCTOS
-- ------------------------------------------------------
-- Server version	8.3.0

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `fabricacion`
--

DROP TABLE IF EXISTS `fabricacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fabricacion` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_producto` int DEFAULT NULL,
  `id_tienda` int DEFAULT NULL,
  `date_inicio` date NOT NULL,
  `date_termino` date DEFAULT NULL,
  `terminado` tinyint(1) DEFAULT '0',
  `del` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id_producto` (`id_producto`),
  CONSTRAINT `fabricacion_ibfk_1` FOREIGN KEY (`id_producto`) REFERENCES `productos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fabricacion`
--

LOCK TABLES `fabricacion` WRITE;
/*!40000 ALTER TABLE `fabricacion` DISABLE KEYS */;
INSERT INTO `fabricacion` VALUES (1,1,1,'2023-11-25',NULL,0,0),(2,2,1,'2023-11-25',NULL,0,0),(3,3,1,'2023-11-25',NULL,0,0);
/*!40000 ALTER TABLE `fabricacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productos`
--

DROP TABLE IF EXISTS `productos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `productos` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_tienda` int DEFAULT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `precio` int DEFAULT NULL,
  `descripcion` tinytext,
  `url_imagen` text,
  `estado` tinyint(1) DEFAULT '1',
  `del` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productos`
--

LOCK TABLES `productos` WRITE;
/*!40000 ALTER TABLE `productos` DISABLE KEYS */;
INSERT INTO `productos` VALUES (1,1,'cajita pokeball',5000,'cajita con forma de pokeball','https://z-p3-scontent.fkna2-1.fna.fbcdn.net/v/t39.30808-6/354047593_734358482029281_7383568526774051176_n.jpg?_nc_cat=108&ccb=1-7&_nc_sid=5f2048&_nc_eui2=AeHofNScdsOIwsLpg3oNFFw9cqGhRk1bhHxyoaFGTVuEfA4-9v9mV6K7Jjs7aq5pI0OGChChigmm9jMTrHQ_h_qc&_nc_ohc=tKcVQGrUMNEAX_gowpT&_nc_zt=23&_nc_ht=z-p3-scontent.fkna2-1.fna&oh=00_AfAdnA04_19AdvXaq1AoC3DIZZMQHZfG6bbPyykQ7i-x3A&oe=6566BA72',1,0),(2,1,'cake topper santiago wanderers',8000,'cake topper santiago wanderers','https://z-p3-scontent.fkna2-1.fna.fbcdn.net/v/t39.30808-6/354831889_737700361695093_657318618107590028_n.jpg?_nc_cat=111&ccb=1-7&_nc_sid=5f2048&_nc_eui2=AeFxBHF5K3oIquVWou2ZfH_OKASgyFVF-dsoBKDIVUX528KH0MJ6pnjpUS-66SGky9OfcIMrWLTh4x0TV4bnrA0I&_nc_ohc=Tz3s5TrvQJIAX8TgExH&_nc_zt=23&_nc_ht=z-p3-scontent.fkna2-1.fna&oh=00_AfARPoPYpWr3uFDJUywIuxH0UhshTijhKP5DVmGOlBRsaw&oe=65674100',1,0),(3,1,'caja san valentín',10000,'caja san valentín','https://z-p3-scontent.fkna2-1.fna.fbcdn.net/v/t39.30808-6/342020989_620925712821055_5152389520039795516_n.jpg?_nc_cat=103&ccb=1-7&_nc_sid=5f2048&_nc_eui2=AeG4fVAOEnVhqXJiqY2mR2-dqpE2wZEzf3eqkTbBkTN_d0FL97ipbTJKMpWakPeounOw9pEry6cfw8vPvrjI-g7Y&_nc_ohc=vJ3_QfrEo2gAX_2ftyU&_nc_zt=23&_nc_ht=z-p3-scontent.fkna2-1.fna&oh=00_AfBdqH9dZ58UJQrZlNu68c_JGnqpCzdwqKCHUC4lQ_QaTQ&oe=65668199',1,0);
/*!40000 ALTER TABLE `productos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productos_insumos`
--

DROP TABLE IF EXISTS `productos_insumos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `productos_insumos` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_producto` int DEFAULT NULL,
  `id_tienda` int DEFAULT NULL,
  `material` varchar(50) NOT NULL,
  `cantidad` int NOT NULL,
  `del` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id_producto` (`id_producto`),
  CONSTRAINT `productos_insumos_ibfk_1` FOREIGN KEY (`id_producto`) REFERENCES `productos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productos_insumos`
--

LOCK TABLES `productos_insumos` WRITE;
/*!40000 ALTER TABLE `productos_insumos` DISABLE KEYS */;
INSERT INTO `productos_insumos` VALUES (1,1,1,'cartulina roja 30x30cm',1,0),(2,1,1,'cartulina blanca 30x30cm',1,0),(3,1,1,'cartulina negra 10x20cm',1,0),(4,2,1,'goma eva glitter verde 30x30cm',1,0),(5,2,1,'goma eva glitter plateada 30x30cm',1,0),(6,2,1,'goma eva blanca 30x30cm',1,0),(7,2,1,'goma eva verde 30x30cm',1,0),(8,2,1,'goma eva negra 30x30cm',1,0),(9,3,1,'cartulina rosa 50x50cm',1,0),(10,3,1,'cartulina roja 30x30cm',1,0);
/*!40000 ALTER TABLE `productos_insumos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'BD06_PRODUCTOS'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-05-01 22:12:02
