# Grupo 2

Este es el repositorio del grupo 2, cuyos integrantes son:

- Nicolás Armijo Calquín - ROL: 202173004-4
- Benjamín Camus Valdés - ROL: 202173072-9
- Valentina Castillo Valdés - ROL: 202021006-3
- Gaspar Navarro Cornejo - ROL: 202173003-6

> **Tutor**: Sofía Martinez


Se continuará trabajando con el proyecto de **Veysa Manualidades**, que corresponde al proyecto número 1.

## Wiki
Puede acceder a la Wiki mediante el siguiente [enlace](https://gitlab.com/valnhe/inf236-grupo06-2023-2/-/wikis/home). 

## Videos

**2023-2**
- [VIDEO - Presentación Cliente](https://www.youtube.com/watch?v=LHmE2B2Bumw)
- [VIDEO - Avance 1](https://www.youtube.com/watch?v=utjX1B0qSZE)
- [VIDEO - Presentación Prototipo](https://youtu.be/pow5RZSBQz8)
- [VIDEO - Presentación Final](https://youtu.be/25jjVhvr9_Q)

**2024-1**
- [VIDEO - Avance](https://youtu.be/U7AL3DZzUVk)

## Aspectos técnicos relevantes

Uno de los aspectos técnicos que debemos tener en consideración es la decisión de separar al emprendedor y al empleado como entidades individuales en nuestro diagrama de contexto, y, por lo tanto, en el resto del proyecto. Esta elección se fundamenta principalmente en nuestra visión de las proyecciones futuras del emprendimiento, que contempla la posibilidad de que se sumen más miembros al emprendimiento.

Es importante destacar que esta separación no se trata de un enfoque rígido. Somos conscientes de que, en ciertas circunstancias particulares, podría ser más adecuado considerar ambas entidades como una sola.

### Descripción técnica

En el proyecto se está utilizando el popular stack MERN, que abarca MongoDB como base de datos (no requerimos implementar una bd para nuestra web app con las funcionalidades que desarrollamos), Express para el backend, React para el frontend, y Node.js como entorno de ejecución del servidor.

#### Sobre las tecnologías y herramientas usadas:

- **Vite:** Vite es el entorno de desarrollo para la página web. Esto nos facilita el desarrollo y mejora el rendimiento durante la fase de desarrollo.
- **React:** React es la tecnología usada para el frontend. La versión específica de React utilizada en este proyecto es la 18.2.0. Además, se ha hecho uso de las dependencias de React DOM para gestionar las rutas web, proporcionando una estructura eficiente y modular al frontend.
- **JavaScript**: JavaScript corresponde al lenguaje de programación principal utilizado en el desarrollo del proyecto.

#### Requerimientos del entorno de desarrollo:

- **Node.js:** Se utiliza la versión 20.9.0 de Node.js para el entorno de desarrollo del frontend y backend de la web app, mientras que para el desarrollo de las APIs se utilizó la version 18.
- **Docker:** La instalación de Docker es esencial para manejar los contenedores y facilitar la implementación y ejecución del proyecto.

## Levantamiento del Proyecto

**Si es primera vez que se debe levantar** el proyecto seguir los pasos desde 1, en caso contrario comenzar desde 2:

1. Levantar un contenedor con mysql para el almacenamiento de datos de los microservicios. Utilizaremos docker compose para levantar todos nuestros servicios, para ello debemos estar en el directorio /inf236-grupo06-2023-2/ y ejecutar el siguiente comando en la terminal:

        docker compose up mysql -d

    Esto levantará un contenedor con mysql, el cual deberemos crear las base de datos necesarias para el funcionamiento de nuestros servicios. 

    Ejecutar el siguiente comando en la terminal:

        docker exec -it inf236-grupo06-2023-2-mysql-1 mysql -uroot -p

    El contenedor le pedirá la contraseña, la cual será "password", una vez ingresada, crearemos las dos bases de datos necesarias con los siguientes comandos: 

        create database BD06_STOCK;

    y 

        create database BD06_PRODUCTOS;

    Una vez terminado ingresaremos "exit" para volver a nuestra terminal. A continuación, ingresar los siguientes comandos, los cuales abrirán la shell del contenedor 
    
        docker exec -it inf236-grupo06-2023-2-mysql-1 sh

    y cargarán los DUMP.sql de cada base de datos, para así tener información de base con la cual trabajar:

        mysql -uroot -p BD06_PRODUCTOS < BD06_PRODUCTOS_DUMP.sql

    El comando nos pedirá ingresar nuevamente una contraseña, ingresaremos "password". Y realizamos lo mismo para el otro servicio:

        mysql -uroot -p BD06_STOCK < BD06_STOCK_DUMP.sql
        
    Terminaremos este paso ingresando "exit" para cerrar la terminal.

2. Una vez realizado el paso 1 en caso de ser necesario (**sólo se debe hacer 1 vez**), continuar con el siguiente. Se deberá ajustar los siguientes archivos, debido a que la implementación de una tecnología (axios - para llamada de apis realizado desde nodejs) requiere una dirección ip la cual no puede ser entregada por 'localhost'. El archivos a modificar es el ".env" que se encuentra en el directorio "/inf236-grupo06-2023-2/WEB_APP/backend/", donde se deberá ingresar la dirección ipv4 a la cual se encuentra conectado el dispositivo en que se ejecutará el levantamiento, para conocer tal dirección se adjunta el siguiente [link](https://support.microsoft.com/en-us/windows/find-your-ip-address-in-windows-f21a9bbc-c582-55cd-35e0-73431160a1b9).

3. Finalmente ingresar el siguiente comando para levantar todos los servicios en conjunto: 

        docker compose up --build -d

    Podrá utilizar los servicios en los siguientes puertos de su localhost (en caso de necesitar ver los logs de los contenedores en la terminal, ejecutar comando sin al argumento "-d"):

    - Frontend: Port 8000.
    - Backend: Port 8082.
    - Api_Stock: Port 8080.
    - Api_Productos: Port 8081.

    Para bajar los servicios basta con colocar:
    
        docker compose down

</details>


> **Enjoy!** - Grupo 06

