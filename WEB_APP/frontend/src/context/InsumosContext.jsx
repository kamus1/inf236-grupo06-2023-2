import React, { createContext, useState } from "react";

export const MyContext = createContext();

export function MyProvider({ children }) {
  const [showB, setShowB] = useState(false);

  return (
    <MyContext.Provider value={{ showB, setShowB }}>
      {children}
    </MyContext.Provider>
  );
}
