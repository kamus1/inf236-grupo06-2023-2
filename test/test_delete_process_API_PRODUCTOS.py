import unittest
import requests
import mysql.connector 

class testDeleteProccesById(unittest.TestCase):
    valid_source_delete_process_request_data = None
    invalid_source_delete_process_request_data = None
    
    conexion_bd_parametros = {
        "host": "localhost",
        "port": 3306,
        "user": "root",
        "password": "password",
        "database": "BD06_PRODUCTOS"
    }
    
    #datos para crear process de prueba
    nuevo_proceso_test = {
        "id": -1,
        "id_producto": 1,
        "id_tienda": 1,
        "date_inicio": "2024-05-09",
        "date_termino": None,
        "terminado": 0,
        "del": 0
    }

    
    @classmethod
    def setUpClass(cls):
        cls.base_url = "http://localhost:8081/api/fabricacion/"
        
        cls.valid_source_delete_process_request_data =  -1 #ID del proceso a borrar, id -1 es el proceso de prueba
        cls.invalid_source_delete_process_request_data = 999999 #ID invalido
        
        
        #conexión con la bd para crear el "process" de prueba
        try:
            conexion = mysql.connector.connect(**cls.conexion_bd_parametros)

            #verificar si la conexión fue exitosa
            if conexion.is_connected():
                print("Conexión exitosa")

            #realizar consulta SQL para insertar el nuevo elemento
            cursor = conexion.cursor()
            consulta = "INSERT INTO fabricacion (id, id_producto, id_tienda, date_inicio, date_termino, terminado, del) VALUES (%s, %s, %s, %s, %s, %s, %s)"
            valores = (cls.nuevo_proceso_test["id"], cls.nuevo_proceso_test["id_producto"], cls.nuevo_proceso_test["id_tienda"], cls.nuevo_proceso_test["date_inicio"], cls.nuevo_proceso_test["date_termino"], cls.nuevo_proceso_test["terminado"], cls.nuevo_proceso_test["del"])
            cursor.execute(consulta, valores)

            #confirmar la operación
            conexion.commit()
            print("Elemento process insertado exitosamente")

        except mysql.connector.Error as error:
            print("Error al conectar a la base de datos:", error)

        finally:
            #cerrar conexión
            if 'cursor' in locals():
                cursor.close()
            if 'conexion' in locals() and conexion.is_connected():
                conexion.close()
                
        
    @classmethod
    def tearDownClass(cls):
        del cls.valid_source_delete_process_request_data
        del cls.invalid_source_delete_process_request_data
        
        #borrar el elemento de ID -1
        try:
            conexion = mysql.connector.connect(**cls.conexion_bd_parametros)
            if conexion.is_connected():
                print("Conexión exitosa")

            #crear cursor para ejecutar consultas sql
            cursor = conexion.cursor()

            #eliminar el elemento con ID -1
            consulta = "DELETE FROM fabricacion WHERE id = %s"
            valores = (-1,)
            cursor.execute(consulta, valores)

            #confirmar
            conexion.commit()
            print("Elemento test con ID -1 borrado exitosamente")

        except mysql.connector.Error as error:
            print("Error al conectar a la base de datos:", error)

        finally:
            #cerrar la conexión
            if 'cursor' in locals():
                cursor.close()
            if 'conexion' in locals() and conexion.is_connected():
                conexion.close()

        
        
    def test_delete_process_valid_id(self):
        response = requests.delete(self.base_url + str(self.valid_source_delete_process_request_data))

        data = response.json()
        message = data["affectedRows"]
        self.assertEqual(message, 1) # 1 fila afectada, proceso borrado correctamente
        
    def test_delete_process_invalid_id(self):
        response = requests.delete(self.base_url + str(self.invalid_source_delete_process_request_data))
    
        data = response.json()
        message = data["affectedRows"]
        self.assertEqual(message, 0) # 0 filas afectadas, no se ha borrado ningún proceso, id invalido

if __name__ == '__main__':
    unittest.main()