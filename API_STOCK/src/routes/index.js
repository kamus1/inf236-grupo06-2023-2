const express = require('express');
const router = new express.Router();

const index = require('../controllers/index');
const stockController = require('../controllers/stockController');

// Esto es un ejemplo - SE PUEDE BORRAR
router.get('/', (req, res) => res.json({message: 'Probando... La prueba fue un éxito API Stock!'}));

//==========================endpoints(Routes)============================//
router.get('/createTableStock', index.createTableStock);

//Stock Routes
router.post('/api/stock', stockController.createStock);
router.get('/api/stock', stockController.getStock);
router.get('/api/stock/insumo/:id', stockController.getStockById);
router.get('/api/stock/insumo',stockController.getStockByName);
router.get('/api/stock/insumos',stockController.getStockSearchByName);
router.put('/api/stock/insumo/:id', stockController.updateStockById);
router.delete('/api/stock/insumo/:id', stockController.deleteStockById);
router.get('/api/stock/tienda/:id', stockController.getStockByTiendaId);


module.exports = router;