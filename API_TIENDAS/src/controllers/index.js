const database = require('../db');


const createTableTiendas = (req, res) => {
    const query =  'CREATE TABLE IF NOT EXISTS tiendas(id INT AUTO_INCREMENT, nombre VARCHAR(100), descripcion TINYTEXT, url_logo TEXT, url_redes TEXT, del BOOL DEFAULT 0, PRIMARY KEY(id))';

    database.query(query, (err) => {
        if (err) throw err;
        res.send('Tabla creada')
    });
};

const createTableCategorias = (req, res) => {
    const query =  'CREATE TABLE IF NOT EXISTS categorias(id INT AUTO_INCREMENT, nombre VARCHAR(100) NOT NULL, descripcion TINYTEXT NOT NULL, del BOOL DEFAULT 0, PRIMARY KEY(id))';

    database.query(query, (err) => {
        if (err) throw err;
        res.send('Tabla creada')
    });
};

const createTableTiendasCategorias = (req, res) => {
    const query =  'CREATE TABLE IF NOT EXISTS tiendas_categorias(id INT AUTO_INCREMENT, id_tienda INT, id_categoria INT, del BOOL DEFAULT 0, PRIMARY KEY(id), FOREIGN KEY(id_tienda) REFERENCES tiendas(id) ON DELETE CASCADE, FOREIGN KEY(id_categoria) REFERENCES categorias(id) ON DELETE CASCADE)';

    database.query(query, (err) => {
        if (err) throw err;
        res.send('Tabla creada')
    });
};

module.exports = {
    createTableTiendas,
    createTableCategorias,
    createTableTiendasCategorias
};