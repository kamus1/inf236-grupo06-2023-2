import unittest
import requests
import mysql.connector 

class testCrearProducto(unittest.TestCase):
    valid_source_create_product_request_data = None
    invalid_source_create_product_request_data = None
    
    test_product_id = None
    
    conexion_bd_parametros = {
        "host": "localhost",
        "port": 3306,
        "user": "root",
        "password": "password",
        "database": "BD06_PRODUCTOS"
    }
    
    @classmethod
    def setUpClass(cls):
        cls.base_url = "http://localhost:8081/api/catalogo"
        cls.test_product_id = None
        
        cls.valid_source_create_product_request_data = {
            "id_tienda": 1,
            "nombre": "Producto test",
            "descripcion": "Descripcion del producto 1",
            "precio": 100,
            "url_imagen":"url_test"
        }
        
        cls.invalid_source_create_product_request_data = {
            "id_tienda": 1,
            "nombre": "Producto test",
            "descripcion": "Descripcion del producto 1",
            "precio": "aasd", #ERROR: precio inválido, debe ser un número
            "url_imagen":"url_test"
        }
    
    @classmethod
    def tearDownClass(cls):
        del cls.valid_source_create_product_request_data
        del cls.invalid_source_create_product_request_data
        
    #borrar el producto de prueba si se creó
        if cls.test_product_id:
            try:
                conexion = mysql.connector.connect(**cls.conexion_bd_parametros)

                #verificar si la conexión
                if conexion.is_connected():
                    print("Conexión exitosa")

                #crear el cursor para ejecutar consultas SQL
                cursor = conexion.cursor()

                #ejecutar la consulta para eliminar el producto con el ID guardado
                consulta = "DELETE FROM productos WHERE id = %s"
                valores = (cls.test_product_id,)
                cursor.execute(consulta, valores)

                #confirmar la operación
                conexion.commit()
                print(f"Producto con ID {cls.test_product_id} eliminado exitosamente de la base de datos")

            except mysql.connector.Error as error:
                print("Error al conectar a la base de datos:", error)

            finally:
                #cerrar el cursor y la conexión
                if 'cursor' in locals():
                    cursor.close()
                if 'conexion' in locals() and conexion.is_connected():
                    conexion.close()

            
        
    def test_create_product_valid_data(self):
        response = requests.post(self.base_url, json=self.valid_source_create_product_request_data)

        data = response.json()
        message = data["message"]
        self.assertEqual(message, "Elemento insertado con éxito!")
        
        #asignar el id del producto para borrarlo en tearDownClass
        self.__class__.test_product_id = data.get("id_producto")
    
        
    def test_create_product_invalid_data(self):
        response = requests.post(self.base_url, json=self.invalid_source_create_product_request_data)
    
        message = response.json()["message"]
        self.assertEqual(message, "Error: tipo de dato incorrecto")
        
if __name__ == '__main__':
    unittest.main()