//aaaaa
const axios = require('axios').default;

//view insumos
const createStock = async (req, res) => {
    try {
        const response = await axios.post('http://localhost:8080/api/stock', req.body);
        res.json(response.data);
    } catch (error) {
        res.json(error);
    }
};

const getInsumos = async (req,res) => {
    try { 
        const response = await axios.get(`http://${process.env.IP}:8080/api/stock/`);
        res.json(response.data);
    } catch (error) {
        res.json(error);
    }

};

const getInsumosById = async (req,res) => {
    const id = req.params['id']
    try {
        const response = await axios.get(`http://${process.env.IP}:8080/api/stock/insumo/${id}`);
        res.json(response.data);
    } catch (error) {
        res.json(error);
    }

};

const getInsumosSearchByName = async (req, res) => {
    const search = req.params['search'];
    const idTienda = req.params['id_tienda'];
    let busqueda = search.toString().replaceAll(' ', '-');
    try {
        const response = await axios.get(`http://${process.env.IP}:8080/api/stock/insumos`, { params: { nombre: busqueda, id_tienda: idTienda }});
        res.json(response.data);
    } catch (error) {
        res.json(error);
    }
};

const getInsumosByTiendaId = async (req,res) => {
    const id = req.params['id']
    try {
        const response = await axios.get(`http://${process.env.IP}:8080/api/stock/tienda/${id}`);
        res.json(response.data);
    } catch (error) {
        res.json(error);
    }

};

//view productos
const createProducto = async (req, res) => {

    const body = req.body

    try {
        const create = await axios.post(`http://${process.env.IP}:8081/api/catalogo`, {id_tienda: body.id_tienda, nombre: body.nombre, precio: body.precio, descripcion: body.descripcion, url_imagen: body.url_imagen});
    
        let promises = [];
        for (i = 0; i < body.insumosSeleccionados.length; i++) {
            promises.push(
            axios.post(`http://${process.env.IP}:8081/api/catalogo/producto/insumos/material` , {id_tienda: body.id_tienda, id_producto: create.data.id_producto, material: body.insumosSeleccionados[i].nombre, cantidad: body.insumosSeleccionados[i].cantidad})
            )
        }

        Promise.all(promises).then(() => res.json({message: "Elemento insertado con éxito"}));
        
    } catch (error) {
        res.json(error)
    }
};

const getProductos = async (req, res) => {
    try {
        const response = await axios.get(`http://${process.env.IP}:8081/api/catalogo`);
        res.json(response.data);
    } catch (error) {
        res.json(error);
    }
};

const getProductosByName = async (req, res) => {
    const search = req.params['search'];
    const idTienda = req.params['id_tienda'];
    let busqueda = search.toString().replaceAll(' ', '-');
    try {
        const response = await axios.get(`http://${process.env.IP}:8081/api/catalogo/producto/tiendas/${idTienda}/`, {params: {nombre: busqueda }});
        res.json(response.data);
    } catch (error) {
        res.json(error);
    }
};

const getProductoById = async (req, res) => {
    const id = req.params['id']
    try {
        const response_prod = (await axios.get(`http://${process.env.IP}:8081/api/catalogo/producto/${id}`)).data[0];
        const response_mat = (await axios.get(`http://${process.env.IP}:8081/api/catalogo/producto/${id}/material`)).data;

        const response = { "producto": response_prod, "materiales": response_mat};
        res.json([response]);
    } catch (error) {
        res.json(error);
    }
};

const getProductosByTiendaId = async (req, res) => {
    const id = req.params['id']
    try {
        const response = await axios.get(`http://${process.env.IP}:8081/api/catalogo/producto/tienda/${id}`);
        res.json(response.data);
    } catch (error) {
        res.json(error);
    }
};

//view fabricacion
const getProceso = async (req, res) => {
    try {
        const response = await axios.get(`http://${process.env.IP}:8081/api/fabricacion/activos`);    
        res.json(response.data);
    } catch (error) {
        res.json(error);
    }
};

const getProcesoTerminado = async (req, res) => {
    try {
        const response = await axios.get(`http://${process.env.IP}:8081/api/fabricacion/terminados`);
        res.json(response.data);
    } catch (error) {
        res.json(error);
    }
};

const fabricarProducto = async (req, res) => {

    const id_producto = req.params.id_producto
    const {id_tienda ,date_inicio} = req.body

    try {
        const mat = (await axios.get(`http://${process.env.IP}:8081/api/catalogo/producto/${id_producto}/material`)).data;

        let promises = []
        for (i = 0; i < mat.length; i++) {
            var nombre = mat[i].material.toString().replaceAll(' ', '-');
            promises.push(
            axios.get(`http://${process.env.IP}:8080/api/stock/insumo` , {params: {nombre: nombre }})
            )
        }

        const responses = await Promise.all(promises);

        let promises_2 = []
        for (i = 0; i < mat.length; i++) {
            var n_disponible = responses[i].data[0].n_disponible - mat[i].cantidad;
            var n_reservado = responses[i].data[0].n_reservado + mat[i].cantidad;
            promises_2.push(
                axios.put(`http://${process.env.IP}:8080/api/stock/insumo/${responses[i].data[0].id}`, {nombre : responses[i].data[0].nombre, material: responses[i].data[0].material, detalle: responses[i].data[0].detalle, n_disponible: n_disponible, n_reservado: n_reservado, n_usado: responses[i].data[0].n_usado})
            )   
        } 
        
        const resp = await Promise.all(promises_2);

        const fabr = await axios.post(`http://${process.env.IP}:8081/api/fabricacion`, {id_tienda:id_tienda, id_producto: id_producto, date_inicio: date_inicio});

        res.json({message: "producto agregado con éxito a procesos"});
    } catch (error) {
        res.json(error);
    }
};

const fabricarProductoDelete = async (req, res) => {

    const id = req.params.id

    try {
        const proc = (await axios.get(`http://${process.env.IP}:8081/api/fabricacion/proceso/${id}`)).data[0];

        const prod = (await axios.get(`http://${process.env.IP}:8081/api/catalogo/producto`, {params: {nombre: proc.nombre}})).data[0];

        const mat = (await axios.get(`http://${process.env.IP}:8081/api/catalogo/producto/${prod.id}/material`)).data;

        let promises = []
        for (i = 0; i < mat.length; i++) {
            var nombre = mat[i].material.toString().replaceAll(' ', '-');
            promises.push(
            axios.get(`http://${process.env.IP}:8080/api/stock/insumo` , {params: {nombre: nombre }})
            )
        }

        const responses = await Promise.all(promises);

        let promises_2 = []
        for (i = 0; i < mat.length; i++) {
            var n_disponible = responses[i].data[0].n_disponible + mat[i].cantidad;
            var n_reservado = responses[i].data[0].n_reservado - mat[i].cantidad;
            promises_2.push(
                axios.put(`http://${process.env.IP}:8080/api/stock/insumo/${responses[i].data[0].id}`, {nombre : responses[i].data[0].nombre, material: responses[i].data[0].material, detalle: responses[i].data[0].detalle, n_disponible: n_disponible, n_reservado: n_reservado, n_usado: responses[i].data[0].n_usado})
            )   
        } 
        
        const resp = await Promise.all(promises_2);

        const fabr = await axios.delete(`http://${process.env.IP}:8081/api/fabricacion/${id}`);

        res.json({message: "proceso eliminado con éxito"});
    } catch (error) {
        res.json(error);
    }
};

const fabricarProductoTerminar = async (req, res) => {

    const id = req.params.id
    const date_termino = req.body.date_termino

    try {
        const proc = (await axios.get(`http://${process.env.IP}:8081/api/fabricacion/proceso/${id}`)).data[0];

        const prod = (await axios.get(`http://${process.env.IP}:8081/api/catalogo/producto`, {params: {nombre: proc.nombre}})).data[0];

        const mat = (await axios.get(`http://${process.env.IP}:8081/api/catalogo/producto/${prod.id}/material`)).data;

        let promises = []
        for (i = 0; i < mat.length; i++) {
            var nombre = mat[i].material.toString().replaceAll(' ', '-');
            promises.push(
            axios.get(`http://${process.env.IP}:8080/api/stock/insumo` , {params: {nombre: nombre }})
            )
        }

        const responses = await Promise.all(promises);

        let promises_2 = []
        for (i = 0; i < mat.length; i++) {
            var n_reservado = responses[i].data[0].n_reservado - mat[i].cantidad;
            var n_usado = responses[i].data[0].n_usado + mat[i].cantidad;
            promises_2.push(
                axios.put(`http://${process.env.IP}:8080/api/stock/insumo/${responses[i].data[0].id}`, {nombre : responses[i].data[0].nombre, material: responses[i].data[0].material, detalle: responses[i].data[0].detalle, n_disponible: responses[i].data[0].n_disponible, n_reservado: n_reservado, n_usado: n_usado})
            )   
        } 
        
        const resp = await Promise.all(promises_2);

        const formatDate = (dateString) => {
            const fechaFormateada = new Date(dateString);
            const dia = fechaFormateada.getDate();
            const mes = fechaFormateada.getMonth() + 1;
            const año = fechaFormateada.getFullYear();
            return `${año}-${mes}-${dia}`;
        };

        const fabr = await axios.put(`http://${process.env.IP}:8081/api/fabricacion/${id}`, {id_producto: prod.id, date_inicio: formatDate(proc.date_inicio), date_termino: date_termino, terminado: 1});

        res.json({message: "proceso agregado con éxito a terminados"});
    } catch (error) {
        res.json(error);
    }
};

const getProcesoByTiendaId = async (req, res) => {
    const id = req.params['id']
    try {
        const response = await axios.get(`http://${process.env.IP}:8081/api/fabricacion/tienda/${id}`);
        res.json(response.data);
    } catch (error) {
        res.json(error);
    }
};

//view tiendas
const createTienda = async (req, res) => {
    try {
        const response = await axios.post(`http://${process.env.IP}:8083/api/tienda`, req.body);
        res.json(response.data);
    } catch (error) {
        res.json(error);
    }
};

const getTienda = async (req, res) => {
    try {
        const response = await axios.get(`http://${process.env.IP}:8083/api/tienda`);
        res.json(response.data);
    } catch (error) {
        res.json(error);
    }
}

const getTiendaById = async (req, res) => {
    const id = req.params['id']
    try {
        const response = await axios.get(`http://${process.env.IP}:8083/api/tienda/${id}`);
        res.json(response.data);
    } catch (error) {
        res.json(error);
    }
}

const getTiendaByName = async (req, res) => {
    const nombre = req.params['nombre']
    try {
        const response = await axios.get(`http://${process.env.IP}:8083/api/tiendas`, {params: {nombre: nombre}});
        res.json(response.data);
    } catch (error) {
        res.json(error);
    }
}

const updateTiendaById = async (req, res) => {
    const id = req.params['id']
    try {
        const response = await axios.put(`http://${process.env.IP}:8083/api/tienda/${id}`, req.body);
        res.json(response.data);
    } catch (error) {
        res.json(error);
    }
}

const deleteTiendaById = async (req, res) => {
    const id = req.params['id']
    try {
        const response = await axios.delete(`http://${process.env.IP}:8083/api/tienda/${id}`);
        res.json(response.data);
    } catch (error) {
        res.json(error);
    }
}

const getProcesosActivosByTiendaId = async (req, res) => {
    const id = req.params.id;
    try {
        const response = await axios.get(`http://${process.env.IP}:8081/api/fabricacion/activos/tienda/${id}`);
        res.json(response.data);
    } catch (error) {
        res.json(error);
    }
};

const getProcesosTerminadosByTiendaId = async (req, res) => {
    const id = req.params.id;
    try {
        const response = await axios.get(`http://${process.env.IP}:8081/api/fabricacion/terminados/tienda/${id}`);
        res.json(response.data);
    } catch (error) {
        res.json(error);
    }
};



module.exports = {
    createStock,
    getInsumos,
    getInsumosById,
    getInsumosSearchByName,
    createProducto,
    getProductos,
    getProductosByName,
    getProductoById,
    getProceso,
    getProcesoTerminado,
    fabricarProducto, 
    fabricarProductoDelete,
    fabricarProductoTerminar,
    getInsumosByTiendaId,
    getProductosByTiendaId,
    getProcesoByTiendaId,
    createTienda,
    getTienda,
    getTiendaById,
    getTiendaByName,
    updateTiendaById,
    deleteTiendaById,
    getProcesosActivosByTiendaId,
    getProcesosTerminadosByTiendaId
};
